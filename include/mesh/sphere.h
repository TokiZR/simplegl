/*******************************************************************************
 * generator.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MESH_SPHERE_H__
#define __MESH_SPHERE_H__ 1

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

#ifndef __MESH_GENERATOR_H__
#include <mesh/generator.h>
#endif

namespace SimpleGL {

class SphereGenerator : public Generator {
public:
	virtual Mesh * create() const;

	Mesh * create(unsigned parallels, unsigned meridians, float radius) const;
};

}

#endif
