/*******************************************************************************
 * pp-compat.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __UTIL_PP_COMPAT_H__
#define __UTIL_PP_COMPAT_H__ 1

//#define PP_COMPAT

#ifdef PP_COMPAT

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

#ifndef __OBJECTS_MESH_H__
#include <objects/mesh.h>
#endif

#ifndef __TriangleMesh_h
#include <pp/TriangleMesh.h>
#endif

namespace SimpleGL {

/**
 * Construct a mesh from a triangle mesh.
 *
 * Unfortunately the format used by the TriangleMesh object (which is
 * inherited from the Wavefront Object format) is incompatible with most
 * modern graphics APIs.
 *
 * For that reason this class exists to convert the mesh into a more
 * digestible format.
 *
 * @param tmesh The triangle mesh.
 * @return The new mesh.
 */
Mesh * fromTriangleMesh(const Graphics::TriangleMesh * tmesh, MeshType type);

}

#endif

#endif
