/*******************************************************************************
 * types.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __UTIL_TYPES_H__
#define __UTIL_TYPES_H__ 1

#include <stdint.h>
#include <stddef.h>

#include <string>

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/quaternion.hpp>

class colour: public glm::vec4 {
public:

	/**
	 * Initialises the colour to opaque white.
	 */
	colour() {
		r = 1;
		g = 1;
		b = 1;
		a = 1;
	}

	/**
	 * Build from a whole 32-bit RGBA colour.
	 */
	colour(uint32_t rgba) {
		r = ((rgba >> 24) & 0xFF) / 255.0;
		g = ((rgba >> 16) & 0xFF) / 255.0;
		b = ((rgba >> 8) & 0xFF) / 255.0;
		a = ((rgba) & 0xFF) / 255.0;
	}

	/**
	 * Create from each component.
	 */
	colour(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255) {
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}
};

using glm::vec2;
using glm::vec3;
using glm::vec4;

using glm::ivec2;
using glm::ivec3;
using glm::ivec4;

using glm::uvec2;
using glm::uvec3;
using glm::uvec4;

using glm::mat2;
using glm::mat3;
using glm::mat4;

using std::string;

namespace SimpleGL {

class Program;
class Shader;
class Scene;
class Object;
class SceneRenderer;
class Mesh;
class Material;
class Light;

struct bounds {
	vec3 min, max;

	bounds() :
			min(NAN), max(NAN) {
	}

	bounds(vec3 min, vec3 max) :
			min(min), max(max) {
	}

	/**
	 * Increments the size of the bounding volume by the specified amount in
	 * all directions.
	 * @param amount
	 */
	inline bounds& enlarge(float amount) {
		min -= vec3(amount);
		max += vec3(amount);
		return *this;
	}

	inline bool intersects(bounds & b) const {
		return !(this->min.x > b.max.x || b.min.x > this->max.x || this->min.y > b.max.y || b.min.y > this->max.y
				|| this->min.z > b.max.z || b.min.z > this->max.z);
	}

	inline bounds merge(bounds& other) const {
		bounds un;
		un.max.x = max.x > other.max.x ? max.x : other.max.x;
		un.min.x = min.x < other.min.x ? min.x : other.min.x;
		un.max.y = max.y > other.max.y ? max.y : other.max.y;
		un.min.y = min.y < other.min.y ? min.y : other.min.y;
		un.max.z = max.z > other.max.z ? max.z : other.max.z;
		un.min.z = min.z < other.min.z ? min.z : other.min.z;
		return un;
	}

	inline bounds& glob(bounds* other) {
		max.x = max.x > other->max.x ? max.x : other->max.x;
		min.x = min.x < other->min.x ? min.x : other->min.x;
		max.y = max.y > other->max.y ? max.y : other->max.y;
		min.y = min.y < other->min.y ? min.y : other->min.y;
		max.z = max.z > other->max.z ? max.z : other->max.z;
		min.z = min.z < other->min.z ? min.z : other->min.z;
		return *this;
	}

	inline bounds& glob(const bounds other) {
		max.x = max.x > other.max.x ? max.x : other.max.x;
		min.x = min.x < other.min.x ? min.x : other.min.x;
		max.y = max.y > other.max.y ? max.y : other.max.y;
		min.y = min.y < other.min.y ? min.y : other.min.y;
		max.z = max.z > other.max.z ? max.z : other.max.z;
		min.z = min.z < other.min.z ? min.z : other.min.z;
		return *this;
	}

	inline bounds& glob(vec3 pt) {
		max.x = max.x > pt.x ? max.x : pt.x;
		min.x = min.x < pt.x ? min.x : pt.x;
		max.y = max.y > pt.y ? max.y : pt.y;
		min.y = min.y < pt.y ? min.y : pt.y;
		max.z = max.z > pt.z ? max.z : pt.z;
		min.z = min.z < pt.z ? min.z : pt.z;
		return *this;
	}

	inline bool contains(vec3 pt) const {
		return !(this->min.x > pt.x || pt.x > this->max.x || this->min.y > pt.y || pt.y > this->max.y || this->min.z > pt.z
				|| pt.z > this->max.z);
	}

	inline bool contains(bounds b) const {
		return min.x <= b.min.x && max.x >= b.max.x && min.y <= b.min.y && max.y >= b.max.y && min.z <= b.min.z
				&& max.z >= b.max.z;
	}

	inline float volume() {
		vec3 box = (max - min);
		return box.x * box.y * box.z;
	}

	inline bounds move(vec3 disp) {
		bounds b(*this);
		b.max += disp;
		b.min += disp;
		return b;
	}
};

}

#endif
