#ifndef __SIMPLE_ARRAY_H__
#define __SIMPLE_ARRAY_H__ 1

#include <malloc.h>

#define DIV_ROUND_UP(dividend, divisor) ((dividend + divisor -1) / divisor)

#define SIMPLE_ARRAY_THRESHOLD_SIZE (1 * 1024 * 1024)
#define SIMPLE_ARRAY_THRESHOLD(type) (SIMPLE_ARRAY_THRESHOLD_SIZE / sizeof(type))
#define SIMPLE_ARRAY_INITIAL_SIZE 4

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

/**
 * Simple union friendly dynamic array.
 */
template<typename T>
class SimpleArray {
private:
	T * data;
	size_t size;
	size_t len;

public:

	/**
	 * Only call init on finalised or uninitialised objects.
	 */
	void init() {
		data = (T*) malloc(sizeof(T) * SIMPLE_ARRAY_INITIAL_SIZE);
		size = SIMPLE_ARRAY_INITIAL_SIZE;
		len = 0;
	}

	/**
	 * Only call finalize on initialised objects.
	 */
	void finalize() {
		free(data);
		data = NULL;
		size = len = 0;
	}

	/**
	 * Add a new value to the end of the array.
	 * @param val
	 */
	void add(T & val) {
		if (size == len) {
			if (size > SIMPLE_ARRAY_THRESHOLD(T)) {
				size += SIMPLE_ARRAY_THRESHOLD(T);
			} else {
				size <<= 1;
			}
			data = (T *) realloc(data, size * sizeof(T));
		}
		data[len++] = val;
	}

	/**
	 * Add an array of new values to the end of the array.
	 * @param val
	 */
	void add(T * vals, unsigned count) {
		if (count == 0) return;
		if (len + count >= size) {
			if (size < SIMPLE_ARRAY_THRESHOLD(T)) {
				size += DIV_ROUND_UP((len + count - size), SIMPLE_ARRAY_THRESHOLD(T)) * SIMPLE_ARRAY_THRESHOLD(T);
			} else {
				while (size < SIMPLE_ARRAY_THRESHOLD(T)) {
					size <<= 1;
				}
				if (len + count >= size) {
					size += DIV_ROUND_UP((len + count - size), SIMPLE_ARRAY_THRESHOLD(T)) * SIMPLE_ARRAY_THRESHOLD(T);
				}
			}

			data = (T *) realloc(data, size * sizeof(T));
		}
		memcpy(data + len, vals, sizeof(T) * count);
		len += count;
	}

	void remove(T & val) {
		size_t i = 0;
		for (; i < len; i++) {
			if (data[i] == val) {
				remove(i);
			}
		}
	}

	T remove(unsigned index) {
		T guy = data[index];
		size_t i = index;
		for (; i < len; i++) {
			data[i] = data[i + 1];
		}

		return guy;
	}

	T & operator[](unsigned index) {
		return data[index];
	}

	const T operator[](unsigned index) const {
		return data[index];
	}

	T & at(unsigned index) {
		return data[index];
	}

	const T at(unsigned index) const {
		return data[index];
	}

	inline T * getData() {
		return data;
	}

	size_t length() const {
		return len;
	}

	size_t capacity() const {
		return size;
	}
};

#endif
