/*******************************************************************************
 * gl.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __UTIL_GL_H__
#define __UTIL_GL_H__ 1

#define GL_GLEXT_PROTOTYPES 1

#ifdef ECLIPSE_CDT
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#else
#include <GL/glew.h>
#endif
#include <GL/freeglut.h>

#include <stdio.h>

#define glCheckError() do {\
	int __errCode = glGetError();\
	if(__errCode) {\
		fprintf(stderr, "%s:%d(%s): OpenGL Error: %s\n",\
			__FILE__, __LINE__, __PRETTY_FUNCTION__,\
			gluErrorString(__errCode));\
	}\
} while(0)

#define glCheck() glCheckError()

#define VERTEX_COORDS_LOCATION			0
#define VERTEX_NORMAL_LOCATION			1
#define VERTEX_MATERIAL_INDEX_LOCATION	2

#endif
