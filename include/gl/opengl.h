#ifndef __GL_OPENGL_H__
#define __GL_OPENGL_H__ 1

#include <util/types.h>
#include <render/shader-types.h>

namespace SimpleGL {

ShadingLanguageType glTypeToHades(int glType);

int glTypeFromHades(ShadingLanguageType hadesType);

}

#endif
