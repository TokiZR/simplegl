/*******************************************************************************
 * gl-renderer.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GL_GL_RENDERER_H__
#define __GL_GL_RENDERER_H__ 1

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

#ifndef __RENDER_RENDERER_H__
#include <render/renderer.h>
#endif

#ifndef __UTIL_GL_H__
#include <util/gl.h>
#endif

#ifndef __HADES_CORE_PROGRAM_H__
#include <gl/gl-program.h>
#endif

#include <deque>

namespace SimpleGL {

class GLRenderer : public SceneRenderer {
private:
	/**
	 * A cached model-view-projection matrix.
	 */
	glm::mat4 mvpMatrix;

	/**
	 * A matrix stack used when rendering.
	 */
	std::deque<glm::mat4> transformStack;

	/**
	 * The timestamp of the camera the last time we rendered, while this stamp
	 * holds our cached mvp will be valid.
	 */
	int cameraVersion;

	/**
	 * Scene dimensions.
	 */
	vec2 dims;

	void checkView();

public:

	GLRenderer();
	~GLRenderer();

	virtual void renderScene();

	void renderMesh(Mesh * m, glm::mat3& transform);



	virtual void setViewport(vec2 dimensions);
	virtual vec2 getViewport();
};

}

#endif
