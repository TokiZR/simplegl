/*******************************************************************************
 * gl-mesh.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GL_GL_MESH_H__
#define __GL_GL_MESH_H__ 1

#ifndef __OBJECTS_MESH_H__
#include <objects/mesh.h>
#endif

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif


#ifndef __UTIL_GL_H__
#include <util/gl.h>
#endif

namespace SimpleGL {

/**
 * Struct containing the buffers that OGL needs to render a mesh.
 */
typedef struct MeshBuffers {
	GLuint dataVAO;
	GLuint elementVBO;
	bool useElements;
} MeshBuffers;

class GLMesh: public Mesh {
private:
	int32_t bufferVersion;

	uint32_t numVertices;
	uint32_t numElements;

	/**
	 * Vertex data buffer.
	 */
	GLuint dataVBO;
	void * mappedData;

	/**
	 * Mesh data on dynamic meshes.
	 */
	vec3 * positions;
	vec3 * normals;
	unsigned * triangles;
	int * materials;

	/**
	 * Mesh elements buffer.
	 */
	GLuint elementsVBO;
	unsigned * mappedElements;

	/**
	 * Vertex Array Object for the mesh.
	 */
	GLuint meshVAO;

	/**
	 * Weather the element buffer is used.
	 */
	bool indirectEnabled;

	void updateOGLBuffers();
public:

	GLMesh(MeshType type);
	virtual ~GLMesh();

	/**
	 * Get the Vertex Array Object which contains all the object information.
	 *
	 * This automatically builds all the buffers and transfers data to the GPU
	 * if there is a version mismatch. You may call this to pre-load the mesh.
	 */
	MeshBuffers getBuffers();

	virtual void update();

	/**
	 * The number of vertices.
	 */
	virtual void setNumVertices(uint32_t count);
	virtual uint32_t getNumVertices();

	/**
	 * The triangles of the mesh.
	 */
	virtual unsigned * getElements();
	virtual void setElements(unsigned * data, uint32_t count);
	virtual uint32_t getElementCount();

	virtual void setIndirectEnabled(bool indirect);

	/**
	 * The vertex positions.
	 */
	virtual vec3 * getPositions();
	virtual void setPositions(vec3 * data);

	/**
	 * The vertex normals.
	 */
	virtual vec3 * getNormals();
	virtual void setNormals(vec3 * data);

	/**
	 * The vertex materials.
	 */
	virtual int * getMaterials();
	virtual void setMaterials(int * data);

	virtual bounds getBounds() const;
};

}

#endif
