/*******************************************************************************
 * shader.h
 * Hades - Lightweight 3D Engine based on GLFW.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HADES_PLATFORM_OPENGL_SHADER_H__
#define __HADES_PLATFORM_OPENGL_SHADER_H__ 1

#ifndef __HADES_CORE_SHADER_H__
#include <render/shader.h>
#endif

namespace SimpleGL {

namespace OpenGL {

class Shader: public SimpleGL::Shader {
private:
	int handle;
public:

	Shader(string name, ShaderType tp) throw(ShaderException);
	virtual ~Shader();

	bool compile();
	virtual string getStatus() const;

	inline int getHandle() const {
		return handle;
	}
};

}

}

#endif
