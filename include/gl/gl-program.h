/*******************************************************************************
 * program.h
 * Hades - Lightweight 3D Engine based on GLFW.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HADES_PLATFORM_OPENGL_PROGRAM_H__
#define __HADES_PLATFORM_OPENGL_PROGRAM_H__ 1

#ifndef __HADES_CORE_PROGRAM_H__
#include <render/program.h>
#endif

namespace SimpleGL {

namespace OpenGL {

/**
 * OpenGL implementation of a program.
 */
class Program: public SimpleGL::Program {
private:
	/**
	 * The OpenGL handle.
	 */
	int handle;

	/**
	 * The uniforms on the program.
	 */
	std::list<UniformInfo> uniforms;

	/**
	 * The status message;
	 */
	string status;

	/**
	 * Pending a re-link due to changes on the shaders.
	 */
	unsigned pendingRelink :1;

	/**
	 * Successfully linked.
	 */
	unsigned linked :1;
public:

	Program();
	virtual ~Program();

	virtual bool linkProgram();
	const virtual string& getStatus();
	virtual void addShader(SimpleGL::Shader * d);
	virtual void removeShader(SimpleGL::Shader * d);
	virtual SimpleGL::Shader * removeShader(ShaderType type, bool unref = true);
	virtual SimpleGL::Shader * getShader(ShaderType tp);
	virtual int getAttributeLocation(const char * attrName) const;
	virtual const std::list<UniformInfo>& getUniforms();
	virtual const UniformInfo* getUniformInfo(const char * name);
	virtual void setUniform(ShadingLanguageType utp, int uniformLoc, void *data,
			int count);
	virtual void getUniform(ShadingLanguageType utp, int uniformLoc, void *data,
			int count);
	virtual void use();

	int getUniformLocation(const char * name);

	inline int getHandle() {
		return handle;
	}
};

}

}

#endif
