/*******************************************************************************
 * quadtree.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __QUADTREE_H__
#define __QUADTREE_H__ 1

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

#ifndef __SIMPLE_ARRAY_H__
#include <util/simple-array.h>
#endif

#ifndef __OBJECTS_MESH_H__
#include <objects/mesh.h>
#endif

namespace SimpleGL {

/**
 * A simple read only quadtree.
 *
 * The tree is constructed upon created and can be then queried.
 *
 * The tree does not support any type of update operation for simplicity.
 */
class Quadtree {
public:
	friend class QNode;

	/**
	 * A node on quadtree.
	 */
	class QNode {
	private:
		bounds nodeBounds;
		union {
			QNode * children[4];
			struct {
				/* Use this to signal we have a leaf. */
				void * pad_leaf_maker;
				/* The size of this is equal to that of 3 pointers. */
				SimpleArray<int> points;
			};
		};

		QNode(bounds b);

		/**
		 * Inserts a point into a node, possibly causing it to split.
		 * @param tree The tree that contains this node.
		 * @param point The point.
		 */
		void insert(Quadtree * tree, int point);

		/**
		 * Splits a node.
		 * @param tree The tree containing the node.
		 *
		 * @return The midpoint of splitting.
		 */
		vec3 split(Quadtree * tree);

	public:
		~QNode();

		inline const QNode * getChild(unsigned quadrant) const {
			if (!isLeaf()) return children[quadrant];
			else return NULL;
		}

		inline const SimpleArray<int> * getPoints() const {
			if (isLeaf()) return &points;
			else return NULL;
		}

		inline const bounds& getBoundingBox() const {
			return nodeBounds;
		}

		inline bool isLeaf() const {
			return pad_leaf_maker == nullptr;
		}

		friend class Quadtree;
	};
private:
	/**
	 * The quadtree's root.
	 */
	QNode * root;

	/**
	 * The points on the quadtree.
	 */
	vec3 * points;

	/**
	 * Number of points total.
	 */
	unsigned pointCount;

	/**
	 * Maximum tree depth.
	 */
	unsigned maxDepth;

	/**
	 * Maximum number of points per node.
	 */
	unsigned maximumPointsPerNode;

	/**
	 * The current insertion depth.
	 */
	unsigned curDepth;

	/**
	 * The total tree depth.
	 */
	unsigned depth;

	/**
	 * The number of nodes in the tree.
	 */
	unsigned numNodes;

	Mesh * treeMesh;

public:
	Quadtree(vec3 * points, unsigned count, unsigned maxDepth, unsigned maximumPointsPerNode);
	~Quadtree();

	struct Query;

	Query * searchPrepare(bounds volume);
	unsigned searchGet(Query * q, int * buffer, unsigned bsize);
	void searchFinish(Query * q);

	inline Mesh * getMesh() const {
		return treeMesh;
	}

	inline const QNode * getRoot() {
		return root;
	}

	inline const vec3 * getData() {
		return points;
	}
};

}

#endif
