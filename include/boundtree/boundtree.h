/*******************************************************************************
 * boundtree.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BOUNDTREE_BOUNDTREE_H__
#define __BOUNDTREE_BOUNDTREE_H__ 1

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

#include <deque>
#include <unordered_map>

namespace SimpleGL {

/**
 * A boundtree is a shorter named AABB Tree.
 *
 * An AABB tree is a type of Bounding Volume Hierarchy tree.
 *
 * It is to an R-Tree what a interval tree is to a BTree. That is: it borrows
 * the overlapping child boundaries concept while sticking to a binary
 * balanced tree.
 *
 * This version attempts to squeeze out a little more performance by allowing
 * the leaf nodes to be n-ary, this can reduce the number of memory accesses
 * and cache misses.
 */
class Boundtree {
private:
	class Node;

	struct node;

	int root;

	std::deque<Node> & nodes;

	unsigned nextEmpty;

	/**
	 * Number of points total.
	 */
	unsigned pointCount;

	/**
	 * Maximum tree depth.
	 */
	unsigned maxDepth;

	/**
	 * Maximum number of points per node.
	 */
	unsigned maxLeaf;

	/**
	 * The current insertion depth.
	 */
	unsigned curDepth;

	/**
	 * The total tree depth.
	 */
	unsigned depth;

	/**
	 * The number of nodes in the tree.
	 */
	unsigned numNodes;

	Mesh * treeMesh;

	bool meshUpToDate :1;

	int splitNode(node n);
	node getNode();
	void releaseNode(unsigned index);

	std::unordered_map<Object *, int> objects;

public:

	Boundtree(unsigned maxDepth, unsigned maximumPointsPerNode);

	struct Query;

	/**
	 * Add a new object to the Boundtree.
	 *
	 * @param obj The object to add.
	 */
	void insert(Object * obj);

	/**
	 * Inform the Boundtree that the coordinates of object obj have changed and
	 * that the tree must be updated.
	 * @param obj
	 */
	bool update(Object * obj);

	/**
	 * Remove an object from the tree.
	 * @param obj The object to be removed.
	 */
	void remove(Object * obj);

	Query * searchPrepare(bounds volume);
	unsigned searchGet(Query * q, Object ** buffer, unsigned bsize);
	void searchFinish(Query * q);

	Mesh * getMesh();

	inline unsigned getDepth() const {
		return depth;
	}

	inline unsigned getNumNodes() const {
		return numNodes;
	}
};

}

#endif
