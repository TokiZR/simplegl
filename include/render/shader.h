/*******************************************************************************
 * shader.h
 * Hades - Lightweight 3D Engine based on GLFW.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HADES_CORE_SHADER_H__
#define __HADES_CORE_SHADER_H__ 1

#include <string>

#include <util/types.h>
#include <render/shader-types.h>

namespace SimpleGL {

typedef enum ShaderType {
	SHADER_TYPE_VERTEX,
	SHADER_TYPE_GEOMETRY,
	SHADER_TYPE_TESSELATION_CONTROL,
	SHADER_TYPE_TESSELATION_EVALUATION,
	SHADER_TYPE_FRAGMENT
} ShaderType;

#define SHADER_NUM_SHADER_TYPES (SHADER_TYPE_FRAGMENT+1)

/**
 * Shader exception.
 */
class ShaderException: public std::exception {
public:
	enum Cause {
		UNSUPORTED, NO_MEMORY, FILE
	};

	ShaderException(Cause c) {
		cause = c;
	}

	Cause getCause() const {
		return cause;
	}

	virtual const char * what() const noexcept(true) {
		switch (cause) {
		case UNSUPORTED:
			return "Unsupported shader type.";
		case NO_MEMORY:
			return "Out of memory.";
		case FILE:
			return "Shader source could not be loaded from the provided file.";
		}
		return "";
	}

protected:
	Cause cause;
};

/**
 * Abstract base class for a shader.
 *
 * Shaders use reference counting and the reference count always starts at 0 so
 * that the first object to reference it becomes it's owner.
 */
class Shader {
protected:
	/**
	 * Name for the shader.
	 *
	 * Helpful for debugging.
	 */
	string name;

	/**
	 * The sources vector, if nSources is 0 then this is a pointer to a single
	 * string.
	 */
	const char ** source;

	const char * singletonSource[1];

	/**
	 * Number of strings in the source buffer.
	 */
	uint16_t nSources;

	/**
	 * Whether this shader has not been recompiled since it was last changed.
	 */
	uint32_t dirty :1;

	/**
	 * The type of the shader.
	 */
	ShaderType type :3;

	/**
	 * Number of references to this shader.
	 */
	int16_t refCount :10;

	/**
	 * Weather the source strings are owned by the shader object and should be leteded with it;
	 */
	bool __ownsSource :1;

	Shader(string name, ShaderType type);
	virtual ~Shader();

public:

	/**
	 * Create a new instance of a shader. This creates a shader for the
	 * currently selected platform.
	 *
	 *
	 * @param type The type of the new shader.
	 * @return The newly created shader.
	 */
	static Shader * create(string name, ShaderType type) throw (ShaderException);

	/**
	 * Create a new shader read from a file.
	 * @param path The file path.
	 * @param type The shader type.
	 * @return The new shader.
	 */
	static Shader * fromFile(const char * path, ShaderType type)
			throw (ShaderException);

	/**
	 * Reference this shader.
	 */
	inline void ref() {
		refCount++;
	}

	/**
	 * Unreference this shader.
	 *
	 * If the reference count drops bellow 1 this object is deleted.
	 *
	 * @return True if the shader is destroyed.
	 */
	inline bool unref() {
		refCount--;
		if (refCount <= 0) {
			delete this;
			return true;
		}
		return false;
	}

	inline ShaderType getType() {
		return type;
	}

	/**
	 * Set the source string for the shader.
	 * @param data The source string.
	 */
	void setSource(const char * data);

	/**
	 * Set an array of string as the shader source.
	 *
	 * The array is copied so that the original may be destroyed.
	 *
	 * @param datav The vector of strings.
	 * @param nDatas The number of strings.
	 */
	void setSource(const char ** datav, uint32_t nDatas = 0);

	/**
	 * Compile the shader, returns true if successful.
	 *
	 * If the compilation was unsuccessful the compileLog method will provide a
	 * driver supplied message explaining the error.
	 *
	 * @return Weather the compilation was successful or not.
	 */
	virtual bool compile() = 0;

	/**
	 * Get the compilation status of this shader.
	 *
	 * @return A message describing what was the status of the last compilation of this shader.
	 */
	virtual string getStatus() const = 0;

	/**
	 * Weather the source text of the shader belongs to it.
	 *
	 * In that case that string would be freed when the shader is destroyed.
	 */
	bool ownsSource() const;
	void ownsSource(bool ownsSource);

	inline string & getName() {
		return name;
	}
};

}

#endif
