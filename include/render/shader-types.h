/*******************************************************************************
 * shader-types.h
 * Hades - Lightweight 3D Engine based on GLFW.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HADES_CORE_SHADER_TYPES_H__
#define __HADES_CORE_SHADER_TYPES_H__ 1

namespace SimpleGL {

/**
 * Recognised uniform types.
 */
typedef enum ShadingLanguageType {
	SLANG_TYPE_INVALID,
	SLANG_TYPE_FLOAT,
	SLANG_TYPE_FLOAT_VEC2,
	SLANG_TYPE_FLOAT_VEC3,
	SLANG_TYPE_FLOAT_VEC4,
	SLANG_TYPE_DOUBLE,
	SLANG_TYPE_DOUBLE_VEC2,
	SLANG_TYPE_DOUBLE_VEC3,
	SLANG_TYPE_DOUBLE_VEC4,
	SLANG_TYPE_INT,
	SLANG_TYPE_INT_VEC2,
	SLANG_TYPE_INT_VEC3,
	SLANG_TYPE_INT_VEC4,
	SLANG_TYPE_UNSIGNED_INT,
	SLANG_TYPE_UNSIGNED_INT_VEC2,
	SLANG_TYPE_UNSIGNED_INT_VEC3,
	SLANG_TYPE_UNSIGNED_INT_VEC4,
	SLANG_TYPE_BOOL,
	SLANG_TYPE_BOOL_VEC2,
	SLANG_TYPE_BOOL_VEC3,
	SLANG_TYPE_BOOL_VEC4,
	SLANG_TYPE_FLOAT_MAT2,
	SLANG_TYPE_FLOAT_MAT3,
	SLANG_TYPE_FLOAT_MAT4,
	SLANG_TYPE_FLOAT_MAT2x3,
	SLANG_TYPE_FLOAT_MAT2x4,
	SLANG_TYPE_FLOAT_MAT3x2,
	SLANG_TYPE_FLOAT_MAT3x4,
	SLANG_TYPE_FLOAT_MAT4x2,
	SLANG_TYPE_FLOAT_MAT4x3,
	SLANG_TYPE_DOUBLE_MAT2,
	SLANG_TYPE_DOUBLE_MAT3,
	SLANG_TYPE_DOUBLE_MAT4,
	SLANG_TYPE_DOUBLE_MAT2x3,
	SLANG_TYPE_DOUBLE_MAT2x4,
	SLANG_TYPE_DOUBLE_MAT3x2,
	SLANG_TYPE_DOUBLE_MAT3x4,
	SLANG_TYPE_DOUBLE_MAT4x2,
	SLANG_TYPE_DOUBLE_MAT4x3,
	SLANG_TYPE_SAMPLER_1D,
	SLANG_TYPE_SAMPLER_2D,
	SLANG_TYPE_SAMPLER_3D,
	SLANG_TYPE_SAMPLER_CUBE,
	SLANG_TYPE_SAMPLER_1D_SHADOW,
	SLANG_TYPE_SAMPLER_2D_SHADOW,
	SLANG_TYPE_SAMPLER_1D_ARRAY,
	SLANG_TYPE_SAMPLER_2D_ARRAY,
	SLANG_TYPE_SAMPLER_1D_ARRAY_SHADOW,
	SLANG_TYPE_SAMPLER_2D_ARRAY_SHADOW,
	SLANG_TYPE_SAMPLER_2D_MULTISAMPLE,
	SLANG_TYPE_SAMPLER_2D_MULTISAMPLE_ARRAY,
	SLANG_TYPE_SAMPLER_CUBE_SHADOW,
	SLANG_TYPE_SAMPLER_BUFFER,
	SLANG_TYPE_SAMPLER_2D_RECT,
	SLANG_TYPE_SAMPLER_2D_RECT_SHADOW,
	SLANG_TYPE_INT_SAMPLER_1D,
	SLANG_TYPE_INT_SAMPLER_2D,
	SLANG_TYPE_INT_SAMPLER_3D,
	SLANG_TYPE_INT_SAMPLER_CUBE,
	SLANG_TYPE_INT_SAMPLER_1D_ARRAY,
	SLANG_TYPE_INT_SAMPLER_2D_ARRAY,
	SLANG_TYPE_INT_SAMPLER_2D_MULTISAMPLE,
	SLANG_TYPE_INT_SAMPLER_2D_MULTISAMPLE_ARRAY,
	SLANG_TYPE_INT_SAMPLER_BUFFER,
	SLANG_TYPE_INT_SAMPLER_2D_RECT,
	SLANG_TYPE_UNSIGNED_INT_SAMPLER_1D,
	SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D,
	SLANG_TYPE_UNSIGNED_INT_SAMPLER_3D,
	SLANG_TYPE_UNSIGNED_INT_SAMPLER_CUBE,
	SLANG_TYPE_UNSIGNED_INT_SAMPLER_1D_ARRAY,
	SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D_ARRAY,
	SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE,
	SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY,
	SLANG_TYPE_UNSIGNED_INT_SAMPLER_BUFFER,
	SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D_RECT
} ShadingLanguageType;

/**
 * Information about a uniform type.
 */
typedef struct ShadingLanguageTypeInfo {
	/**
	 * Type identifier.
	 */
	ShadingLanguageType type;

	/**
	 * Description of the type.
	 */
	const char * readableName;

	/**
	 * The type name in the platform's shadinbg language.
	 */
	const char * shaderTypeName;
} ShadingLanguageTypeInfo;

}

#include <gl/gl-shader-types.h>

namespace SimpleGL {

typedef struct UniformInfo {
	ShadingLanguageType type;
	int32_t location;
	string name;
	bool array :1;
	unsigned arrayLength :31;
} UniformInfo;

}

#endif
