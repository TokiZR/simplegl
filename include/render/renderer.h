/*******************************************************************************
 * renderer.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __RENDER_RENDERER_H__
#define __RENDER_RENDERER_H__ 1

#ifndef __OBJECTS_SCENE_H__
#include <objects/scene.h>
#endif

#include <objects/camera.h>

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif


namespace SimpleGL {

typedef enum RenderMode {
	/**
	 * Regular shape rendering.
	 */
	RENDER_MODE_FILL,

	/**
	 * Wireframe rendering, this simply forces triangles to be rendered as lines.
	 */
	RENDER_MODE_WIREFRAME
} RenderMode;

class SceneRenderer {
protected:
	SceneRenderer();

public:
	/**
	 * The scene to render.
	 */
	Scene * scene;

	/**
	 * Camera viewing the scene.
	 */
	Camera * camera;

	/**
	 * Rendering mode for the scene.
	 */
	RenderMode mode :1;

	static SceneRenderer * create();

	virtual ~SceneRenderer() {
	}

	virtual void renderScene() = 0;

	virtual void setViewport(vec2 dimensions) = 0;
	virtual vec2 getViewport() = 0;
};

}
#endif
