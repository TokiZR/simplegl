/*******************************************************************************
 * program.h
 * Hades - Lightweight 3D Engine based on GLFW.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HADES_CORE_PROGRAM_H__
#define __HADES_CORE_PROGRAM_H__ 1

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

#include <list>

#ifndef __HADES_CORE_SHADER_H__
#include <render/shader.h>
#endif

namespace SimpleGL {

/**
 * A simple program.
 *
 * This program implements a simple interface that is compatible both with
 * OpenGL and DirectX.
 */
class Program {
protected:
	/**
	 * Each of the shader stages.
	 */
	Shader * stages[SHADER_NUM_SHADER_TYPES];

	Program() {
		for (Shader *& st : stages) {
			st = NULL;
		}
	}

public:

	/**
	 * Create a new instance of the program for the current platform.
	 */
	static Program * create();

	virtual ~Program() {
	}

	/**
	 * Link the program.
	 *
	 * Returns true if the linking is successful or if the program was already
	 * linked.
	 *
	 * If linking fails one may use getStatus to find out what the problem is.
	 *
	 * @return Weather linking is successful.
	 */
	virtual bool linkProgram() = 0;

	/**
	 * Get the status of the program.
	 */
	const virtual string& getStatus() = 0;

	/**
	 * Adds a new shader to this program.
	 *
	 * If a shader for the same stage already existed it is detached and
	 * unreferenced. The new shader will be referenced.
	 *
	 * @param d The shader.
	 */
	virtual void addShader(Shader * d) = 0;

	/**
	 * Remove a given shader form the program.
	 * @param d The shader to remove.
	 */
	virtual void removeShader(Shader * d) = 0;

	/**
	 * Remove a given stage from the program.
	 *
	 * When a shader is remove it is unreferenced, this may cause it to be
	 * destroyed, in that case NULL will be returned.
	 *
	 * When there is no shader on the requested stage NULL is also returned.
	 *
	 * @param The type of the shader to remove.
	 * @return The removed shader.
	 */
	virtual Shader * removeShader(ShaderType tp, bool unref = true) = 0;

	/**
	 * Returns the currently assigned shader for the provided stage.
	 *
	 * @param tp The type of the queried shader.
	 *
	 * @return The shader if any or null.
	 */
	virtual Shader * getShader(ShaderType tp) = 0;

	/**
	 * Get information about the location of a input attribute.
	 * @param attrName The name of thwe attribute.
	 * @return The location of the attribute.
	 */
	virtual int getAttributeLocation(const char * attrName) const = 0;

	/**
	 * Get a list of the uniforms this program has.
	 */
	virtual const std::list<UniformInfo>& getUniforms() = 0;

	/**
	 * Get uniform information for a given name.
	 * @param name The name of the uniform.
	 * @return A reference to the uniform information.
	 */
	virtual const UniformInfo* getUniformInfo(const char * name) = 0;

	/**
	 * Set a given uniform.
	 *
	 * @param utp The type of the uniform.
	 * @param uniformLoc The location of the uniform.
	 * @param data The actual data.
	 * @param count The number of members of that data (for arrays of data).
	 */
	virtual void setUniform(ShadingLanguageType utp, int uniformLoc, void *data, int count) = 0;

	/**
	 * @{
	 * Shorthand uniform setters.
	 */

	void setUniform(int uniformLoc, float f0) {
		setUniform(SLANG_TYPE_FLOAT, uniformLoc, &f0, 1);
	}

	void setUniform(int uniformLoc, float f0, float f1) {
		vec2 d(f0, f1);
		setUniform(uniformLoc, d);
	}

	void setUniform(int uniformLoc, float f0, float f1, float f2) {
		vec3 d(f0, f1, f2);
		setUniform(uniformLoc, d);
	}

	void setUniform(int uniformLoc, float f0, float f1, float f2, float f3) {
		vec4 d(f0, f1, f2, f3);
		setUniform(uniformLoc, d);
	}

	void setUniform(int uniformLoc, vec2 & v2) {
		setUniform(SLANG_TYPE_FLOAT_VEC2, uniformLoc, &v2, 1);
	}

	void setUniform(int uniformLoc, vec3 & v3) {
		setUniform(SLANG_TYPE_FLOAT_VEC3, uniformLoc, &v3, 1);
	}

	void setUniform(int uniformLoc, vec4 & v4) {
		setUniform(SLANG_TYPE_FLOAT_VEC4, uniformLoc, &v4, 1);
	}

	void setUniform(int uniformLoc, mat2 & m2) {
		setUniform(SLANG_TYPE_FLOAT_MAT2, uniformLoc, &m2, 1);
	}

	void setUniform(int uniformLoc, mat3 & m3) {
		setUniform(SLANG_TYPE_FLOAT_MAT3, uniformLoc, &m3, 1);
	}

	void setUniform(int uniformLoc, mat4 & m4) {
		setUniform(SLANG_TYPE_FLOAT_MAT4, uniformLoc, &m4, 1);
	}
	/** @} */

	/**
	 * Get the value of a uniform.
	 *
	 * @param utp The type of the uniform.
	 * @param uniformLoc The location of the uniform.
	 * @param data The actual data.
	 * @param count The number of members of that data (for arrays of data).
	 */
	virtual void getUniform(ShadingLanguageType utp, int uniformLoc, void *data, int count) = 0;

	/**
	 * Activate the program.
	 */
	virtual void use() = 0;

};

}

#endif
