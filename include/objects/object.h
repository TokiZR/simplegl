/*******************************************************************************
 * object.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OBJECTS_OBJECT_H__
#define __OBJECTS_OBJECT_H__ 1

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

#ifndef __OBJECTS_MESH_H__
#include <objects/mesh.h>
#endif

#include <list>
#include <deque>
#include <string>

namespace SimpleGL {

/**
 * Is it in the scene? It's an object.
 *
 * Objects are simple scene tree nodes that apply transformations to their
 * children and may optionally hold meshes.
 */
class Object {
private:
	/**
	 * My name :)
	 */
	std::string name;

	/***************************************************************************
	 *
	 * 	Object tree fields.
	 *
	 */

	/**
	 * Next sibling.
	 */
	Object * next;

	/**
	 * Previous sibling.
	 */
	Object * prev;

	/**
	 * Parent object.
	 */
	Object * parent;

	/**
	 * The first child node.
	 */
	Object * firstChild;

	/**
	 * Last child node.
	 */
	Object * lastChild;

	/**
	 * Number of child nodes.
	 */
	size_t nChildren;

	/**
	 * We'll naïvely assume that the bounds only need to be calculated once.
	 */
	bounds b;

	friend class ObjectIterator;

public:
	/**
	 * The rotation of the object, always around the object's relative origin.
	 */
	vec3 rotation;

	/**
	 * The x and y coordinates of the object on it's parent's space.
	 */
	vec3 position;

	/**
	 * The horizontal and vertical scaling of the object.
	 */
	vec3 scale;

	/**
	 * Weather this object is rendered.
	 */
	bool visible :1;

	Object(string name);

	Object(Mesh& mesh, string name);

	Object(Mesh& mesh, Material * mat, string name);

	virtual ~Object() {
	}

	/**
	 * Append a child to this objects child list.
	 */
	void addChild(Object * child);

	/**
	 * Add a child object at a provided index.
	 */
	void addChild(Object * ch, int index);

	/**
	 * remove a child.
	 */
	void removeChild(Object * ch);

	/***************************************************************************
	 *
	 * 	Associated meshes
	 *
	 */

	struct MeshRef {
		Mesh * mesh;
		uint8_t matIndex;

		MeshRef(Mesh * mesh, uint8_t ind) :
				mesh(mesh), matIndex(ind) {
		}
	};

	std::list<MeshRef> meshes;
	std::deque<Material *> materials;

	const string & getName() const {
		return name;
	}

	/**
	 * Weather this object is a descendant of target object.
	 *
	 * @param obj The potential parent.
	 * @return
	 */
	inline bool isDescendantOf(Object * obj) {
		if (obj == this) return false;
		while (parent) {
			if (parent == obj) return true;
		}
		return false;
	}

	/**
	 * Get the extents of an object.
	 */
	bounds getExtents();
};

/**
 * Simple iterator for objects.
 *
 * This is not compatible with C++11's for each loops since I could not decide
 * what would be the right semantic of `for each object in object`.
 */
class ObjectIterator {
private:
	Object * cur;

public:

	/**
	 * Construct a new object iterator for any object.
	 */
	inline ObjectIterator(Object * obj) {
		this->cur = obj;
	}

	/**
	 * Weather the iterator points to a valid object.
	 */
	inline bool isValid() {
		return cur != NULL;
	}

	/**
	 * Weather the current object is visible.
	 *
	 * This also returns false without errors if the iterator is not valid.
	 */
	inline bool isVisible() {
		return isValid() && cur->visible;
	}

	/**
	 * Advance the iterator to the next object.
	 */
	inline ObjectIterator next() {
		return ObjectIterator(cur->next);
	}

	/**
	 * Advance the iterator to the previous object.
	 */
	inline ObjectIterator prev() {
		return ObjectIterator(cur->prev);
	}

	/**
	 * Advance the iterator to the next object.
	 */
	inline ObjectIterator nextVisible() {
		Object * o = cur;
		while (o->next && !o->next->visible)
			o = o->next;
		return ObjectIterator(o->next);
	}

	/**
	 * Advance the iterator to the previous object.
	 */
	inline ObjectIterator prevVisible() {
		Object * o = cur;
		while (o->prev && !o->prev->visible)
			o = o->prev;
		return ObjectIterator(o->prev);
	}

	/**
	 * Advance the iterator to the parent object.
	 */
	inline ObjectIterator parent() {
		return ObjectIterator(cur->parent);
	}

	/**
	 * Advance the iterator to the first child object.
	 */
	inline ObjectIterator firstChild() {
		return ObjectIterator(cur->firstChild);
	}

	/**
	 * Advance the iterator to the last child object.
	 */
	inline ObjectIterator lastChild() {
		return ObjectIterator(cur->lastChild);
	}

	/**
	 * Advance the iterator to the first child object.
	 */
	inline ObjectIterator firstVisibleChild() {
		if (cur->firstChild && !cur->firstChild->visible) return ObjectIterator(cur->firstChild).nextVisible();
		else return ObjectIterator(cur->firstChild);
	}

	/**
	 * Advance the iterator to the last child object.
	 */
	inline ObjectIterator lastVisibleChild() {
		if (cur->lastChild && !cur->lastChild->visible) return ObjectIterator(cur->lastChild).prevVisible();
		else return ObjectIterator(cur->lastChild);
	}

	/**
	 * Get the number of children of this object.
	 */
	inline int childrenCount() {
		return cur->nChildren;
	}

	/**
	 * Get the number of siblings of this object.
	 */
	inline int siblingCount() {
		if (cur->parent) {
			return cur->parent->nChildren;
		} else {
			/* Root is me, so no siblings */
			return 1;
		}
	}

	/**
	 * Cast to object.
	 *
	 * Cast overloads are bad ideas for the most part but in the case of
	 * iterators the result is intuitive.
	 */
	inline operator Object*() {
		return cur;
	}

	/**
	 * Iterator dereference.
	 */
	inline Object* operator *() {
		return cur;
	}

	/**
	 * Iterator pointer dereference.
	 */
	inline Object * operator ->() {
		return cur;
	}

	/**
	 * In-place advance to next, does nothing if already at the last element.
	 *
	 * @return Reference to self.
	 */
	inline ObjectIterator& operator ++() {
		if (cur && cur->next) {
			cur = cur->next;
		}
		return *this;
	}

	/**
	 * In-place advance to previous, does nothing if already at the last element.
	 *
	 * @return Reference to self.
	 */
	inline ObjectIterator& operator --() {
		if (cur && cur->prev) {
			cur = cur->prev;
		}
		return *this;
	}
};

}

#endif
