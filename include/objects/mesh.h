/*******************************************************************************
 * mesh.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OBJECTS_MESH_H__
#define __OBJECTS_MESH_H__ 1

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

namespace SimpleGL {

/**
 * The types of primitives for a mesh.
 */
typedef enum PrimitiveType {
	/**
	 * The primitives are simple points.
	 *
	 * The point radius can be adjusted in the mesh renderer.
	 */
	PRIMITIVE_POINTS,

	/**
	 * Each set of two vertices defines a single line segment.
	 */
	PRIMITIVE_LINE_LIST,

	/**
	 * The first vertex defines a starting point and each following vertex
	 * defines a new line segment composed of this vertex and the previous.
	 */
	PRIMITIVE_LINE_STRIP,

	/**
	 * Just as PRIMITIVE_LINE_STRIP except the last vertex automatically
	 * connects to the first.
	 */
	PRIMITIVE_LINE_LOOP,

	/**
	 * Each three vertices defines an individual triangle.
	 */
	PRIMITIVE_TRIANGLES,

	/**
	 * The first two vertices define a starting line segment and each following
	 * vertex defines a new triangle composed of itself and the previous 2
	 * vertices.
	 */
	PRIMITIVE_TRIANGLE_STRIP,

	/**
	 * The first vertex defines the centre of the fan, the following vertex
	 * defines the first line segment and then each following vertex defines a
	 * new triangle composed of itself, the first vertex and the previous.
	 */
	PRIMITIVE_TRIANGLE_FAN
} PrimitiveType;

/**
 * Hits for mesh usage.
 */
enum MeshType {
	/**
	 * This hint indicates to the underlying graphics API that the mesh should
	 * be stored on GPU memory as it will not be updated.
	 */
	MESH_STATIC,

	/**
	 * This indicates that the mesh is going to be frequently updated, such as
	 * a mesh that is animated.
	 */
	MESH_DYNAMIC
};

/**
 * A simple wrapper fro PP's meshes.
 */
class Mesh {
protected:
	/**
	 * The mesh version.
	 */
	int32_t version;

	MeshType type :1;

	PrimitiveType ptype :3;

	Mesh(MeshType type);

public:
	/**
	 * Create a new triangle mesh.
	 *
	 * The mesh does not use a public constructor since it is actually hiding
	 * it's backend specific implementation.
	 *
	 * @param type The type of the mesh.
	 */
	static Mesh * create(MeshType type);

	virtual ~Mesh() {
	}


	inline void setPrimitiveType(PrimitiveType tp) {
		ptype = tp;
	}
	inline PrimitiveType getPrimitiveType() const {
		return ptype;
	}

	/**
	 * Get the mesh version.
	 *
	 * Every time the mesh is changed the version is increased, this allows any
	 * object that builds up from the mesh to keep track of it without the
	 * conflicts of dirty bits or the overhead of timestamps.
	 */
	inline uint32_t getVersion() {
		return version;
	}

	/**
	 * Whenever the underlying mesh is updated one must call this method
	 * increase the version number.
	 */
	virtual void update();

	/**
	 * The number of vertices.
	 */
	virtual void setNumVertices(uint32_t count) = 0;
	virtual uint32_t getNumVertices() = 0;

	/**
	 * The triangles of the mesh.
	 */
	virtual unsigned * getElements() = 0;
	virtual void setElements(unsigned * data, uint32_t length) = 0;
	virtual uint32_t getElementCount() = 0;

	/**
	 * Set weather indirect rendering is used for this mesh.
	 */
	virtual void setIndirectEnabled(bool indirect) = 0;

	/**
	 * The vertex positions.
	 */
	virtual vec3 * getPositions() = 0;
	virtual void setPositions(vec3 * data) = 0;

	/**
	 * The vertex normals.
	 */
	virtual vec3 * getNormals() = 0;
	virtual void setNormals(vec3 * data) = 0;

	/**
	 * The vertex materials.
	 */
	virtual int * getMaterials() = 0;
	virtual void setMaterials(int * data) = 0;

	virtual bounds getBounds() const = 0;
};

}

#endif
