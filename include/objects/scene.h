/*******************************************************************************
 * scene.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OBJECTS_SCENE_H__
#define __OBJECTS_SCENE_H__ 1

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

#ifndef __OBJECTS_OBJECT_H__
#include <objects/object.h>
#endif

#ifndef __OBJECTS_LIGHT_H__
#include <objects/light.h>
#endif

#include <vector>

namespace SimpleGL {

class Scene {
public:
	/**
	 * The background colour of the scene.
	 */
	colour background;
	colour ambientLight;

	/**
	 * The root object in the scene.
	 */
	Object * root;

	/**
	 * The lights on the scene.
	 */
	std::vector<Light *> lights;

	Scene() : background(1,1,1,1), ambientLight(0.2, 0.2, 0.2, 1.0), lights(){
		/* W00t */
		root = new Object("Scene Root");
	}

	/**
	 * Shorthand method to insert a new child into the root object.
	 */
	inline void addToRoot(Object * obj) {
		root->addChild(obj);
	}
};

}

#endif
