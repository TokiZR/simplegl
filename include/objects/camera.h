/*******************************************************************************
 * camera.h
 * Hades - Lightweight 3D Engine based on GLFW.
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HADES_CORE_CAMERA_H__
#define __HADES_CORE_CAMERA_H__ 1

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

namespace SimpleGL {

typedef enum CameraProjection {
	PROJECTION_PERSPECTIVE, PROJECTION_PARALLEL
} CameraProjection;

class Camera {
private:
	/**
	 * The position of the camera in space.
	 */
	vec3 __position;

	/**
	 * The view direction (normalised).
	 */
	vec3 __viewDir;

	/**
	 * Up direction of the camera (normalised).
	 */
	vec3 __up;

	/**
	 * The ratio between the width and the height of the image the camera
	 * generates.
	 *
	 * For instance in a 16 by 9 monitor the aspect ratio is 16/9 or
	 * approximately 1.77.
	 */
	float __aspectRatio;

	/**
	 * The height of the camera image.
	 *
	 * This is only used in orthographic projection.
	 */
	float __height;

	/**
	 * The horizontal field of view.
	 *
	 * This is only used in perspective projection.
	 */
	float __horizontalFov;

	/**
	 * Near clipping plane.
	 */
	float __nearClip;

	/**
	 * Far clipping plane.
	 */
	float __farClip;

	/**
	 * A cached view matrix.
	 */
	mat4 viewMatrix;

	/**
	 * The type of the projection.
	 */
	CameraProjection __proj :1;

	/**
	 * Weather the camera has been modified, used to refresh the cached view
	 * transform.
	 */
	bool modifyed :1;
public:

	Camera();

	float aspectRatio() const;
	Camera * aspectRatio(float aspectRatio);

	float height() const;
	Camera * height(float height);

	float horizontalFov() const;
	Camera * horizontalFov(float horizontalFov);

	const vec3 position() const;
	Camera * position(vec3 position);

	CameraProjection projectionType() const;
	Camera * projectionType(CameraProjection proj);

	const vec3 up() const;
	Camera * up(vec3 up);

	const vec3 viewDir() const;
	Camera * viewDir(vec3 viewDir);

	/**
	 * Face the camera towards a given point.
	 *
	 * @param position The point to look at.
	 * @param up The upwards direction for the camera image, any linearly
	 * 		independent vector from the view direction.
	 */
	void lookAt(vec3 position, vec3 up);

	mat4& getViewMatrix();

	float farClip() const;
	Camera * farClip(float farClip);

	float nearClip() const;
	Camera * nearClip(float nearClip);

	/**
	 * Translate the camera with respect to the plane determined by it's
	 * position and a normal equal to the viewing direction.
	 */
	void translateFilm(vec3 translation);
};

}

#endif
