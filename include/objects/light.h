/*******************************************************************************
 * light.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OBJECTS_LIGHT_H__
#define __OBJECTS_LIGHT_H__ 1

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

namespace SimpleGL {

/**
 * Type of a light.
 */
typedef enum LightType {
	LIGHT_TYPE_OFF,
	/**
	 * Linear falloff point light.
	 */
	LIGHT_TYPE_POINT_LINEAR,

	/**
	 * Quadratic falloff point light.
	 */
	LIGHT_TYPE_POINT_QUAD,

	/**
	 * Directional light.
	 */
	LIGHT_TYPE_DIRECTIONAL
} LightType;

class Light {
public:
	/**
	 * If the w coordinate is 0 then the light is directional.
	 */
	vec4 position;
	colour col;

	float size;

	LightType type :2;
	bool on :1;

	Light(LightType tp, colour c) :
			position(), col(c), size(1), type(tp), on(true) {
	}

	Light(LightType tp, vec4 pos = vec4(), colour c = colour()) :
			position(pos), col(c), size(1), type(tp), on(true) {
	}
};

}

#endif
