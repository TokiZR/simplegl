/*******************************************************************************
 * material.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OBJECTS_MATERIAL_H__
#define __OBJECTS_MATERIAL_H__ 1

#include <util/types.h>

namespace SimpleGL {

enum class BlendMode {
	Normal, Add, Subtract, Alpha, PremultipliedAlpha
};

class Material {
protected:
	Program * prog;

	inline Material() :
			prog(NULL) {
	}
public:

	virtual ~Material() {
	}

	/**
	 * Get the program associated with this material.
	 */
	virtual Program * getProgram() const {
		return prog;
	}

	/**
	 * Prepare and activate the program for this material.
	 *
	 * @param scene The scene this program will be used in.
	 * @param renderer The renderer that will use the program.
	 *
	 * @return The program in use.
	 */
	virtual Program * useMaterial(Mesh * mesh, Object * object, Scene * scene,
			SceneRenderer * renderer) const {
		return prog;
	}

	virtual BlendMode getBlendMode() const {
		return BlendMode::Normal;
	}
};

}

#endif
