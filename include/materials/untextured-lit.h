/*******************************************************************************
 * untextured-lit.h
 * SimpleGL - Simple OpenGL rendering pipeline
 * Copyright (C) 2014 Daniel Ilha
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MATERIALS_UNTEXTURED_LIT_H__
#define __MATERIALS_UNTEXTURED_LIT_H__ 1

#ifndef __UTIL_TYPES_H__
#include <util/types.h>
#endif

#ifndef __OBJECTS_MATERIAL_H__
#include <objects/material.h>
#endif

namespace SimpleGL {

namespace Materials {

class UntexturedLit: public SimpleGL::Material {
private:
	vec3 __ambient;
	vec3 __diffuese;
	vec3 __spot;
	float __shiness;

public:
	UntexturedLit(colour base);

	virtual Program * getProgram() const;
	virtual Program * useMaterial(Mesh * mesh, Object * object, Scene * scene, SceneRenderer * renderer) const;

	const vec3& ambient() const {
		return __ambient;
	}

	UntexturedLit * ambient(const vec3& ambient) {
		__ambient = ambient;
		return this;
	}

	const vec3& diffuese() const {
		return __diffuese;
	}

	UntexturedLit * diffuese(const vec3& diffuese) {
		__diffuese = diffuese;
		return this;
	}

	float shiness() const {
		return __shiness;
	}

	UntexturedLit * shiness(float shiness) {
		__shiness = shiness;
		return this;
	}

	const vec3& spot() const {
		return __spot;
	}

	UntexturedLit * spot(const vec3& spot) {
		__spot = spot;
		return this;
	}
};

}

}

#endif
