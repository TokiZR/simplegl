#include "objects/mesh.h"

#include "util/gl.h"

#include "gl/gl-mesh.h"

using namespace SimpleGL;

SimpleGL::Mesh::Mesh(MeshType type) {
	this->type = type;
	this->version = 0;
	this->ptype = PRIMITIVE_POINTS;
}

Mesh* SimpleGL::Mesh::create(MeshType type) {
	return new GLMesh(type);
}

void SimpleGL::Mesh::update() {
	version++;
}
