#include "objects/object.h"

#include "materials/flat.h"

using namespace SimpleGL;

SimpleGL::Object::Object(std::string name) :
		name(name), next(NULL), prev(NULL), parent(NULL), firstChild(NULL), lastChild(NULL), nChildren(0), rotation(0, 0, 0), position(
				0, 0, 0), scale(1, 1, 1), visible(true) {
}

void SimpleGL::Object::addChild(Object* child) {
	if (lastChild) {
		lastChild->next = child;
	} else {
		firstChild = lastChild = child;
	}
	child->prev = lastChild;
	lastChild = child;
	child->next = NULL;
	child->parent = this;
	nChildren++;
}

void SimpleGL::Object::addChild(Object* ch, int index) {
}

SimpleGL::Object::Object(Mesh& mesh, string name) :
		Object(mesh, new Materials::Flat(colour(1)), name) {
}

void SimpleGL::Object::removeChild(Object* ch) {
	if (ch->parent == this) {
		if (ch->prev) ch->prev->next = ch->next;
		else firstChild = ch->next;

		if (ch->next) ch->next->prev = ch->prev;
		else lastChild = ch->prev;
	}
}

SimpleGL::Object::Object(Mesh& mesh, Material* mat, string name) :
		name(name), next(NULL), prev(NULL), parent(NULL), firstChild(NULL), lastChild(NULL), nChildren(0), rotation(0, 0, 0), position(
				0, 0, 0), scale(1, 1, 1), visible(true) {
	meshes.push_back(MeshRef(&mesh, 0));
	materials.push_back(mat);
}

bounds SimpleGL::Object::getExtents() {
	if (isnanf(this->b.min.x)) {
		bounds b;

		for (const MeshRef& r : meshes) {
			b.glob(r.mesh->getBounds());
		}
		this->b = b;
	}

	ObjectIterator it = ObjectIterator(this->firstChild);
	while (it.isValid()) {
		b.glob(it->getExtents());
		it = it.next();
	}

	return b.move(position);
}
