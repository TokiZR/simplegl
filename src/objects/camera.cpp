#include <objects/camera.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

using namespace SimpleGL;

SimpleGL::Camera::Camera() :
		__position(0, 0, 0), __viewDir(0, 0, -1), __up(0, 1, 0), __aspectRatio(1), __height(1), __horizontalFov(
		M_PI / 2), __nearClip(0.1), __farClip(10000), viewMatrix(), __proj(PROJECTION_PERSPECTIVE), modifyed(
				true) {
	getViewMatrix();
}

float SimpleGL::Camera::aspectRatio() const {
	return __aspectRatio;
}

Camera * SimpleGL::Camera::aspectRatio(float aspectRatio) {
	__aspectRatio = aspectRatio;
	modifyed = true;
	return this;
}

float SimpleGL::Camera::height() const {
	return __height;
}

Camera * SimpleGL::Camera::height(float height) {
	__height = height;
	modifyed = true;
	return this;
}

float SimpleGL::Camera::horizontalFov() const {
	return __horizontalFov;
}

Camera * SimpleGL::Camera::horizontalFov(float horizontalFov) {
	__horizontalFov = horizontalFov;
	modifyed = true;
	return this;
}

const vec3 SimpleGL::Camera::position() const {
	return __position;
}

Camera * SimpleGL::Camera::position(const vec3 position) {
	__position = position;
	modifyed = true;
	return this;
}

CameraProjection SimpleGL::Camera::projectionType() const {
	return __proj;
}

Camera * SimpleGL::Camera::projectionType(CameraProjection proj) {
	__proj = proj;
	modifyed = true;
	return this;
}

const vec3 SimpleGL::Camera::up() const {
	return __up;
}

Camera * SimpleGL::Camera::up(const vec3 up) {
	__up = up;
	modifyed = true;
	return this;
}

const vec3 SimpleGL::Camera::viewDir() const {
	return __viewDir;
}

Camera * SimpleGL::Camera::viewDir(const vec3 viewDir) {
	__viewDir = viewDir;
	modifyed = true;
	return this;
}

void SimpleGL::Camera::lookAt(vec3 position, vec3 up) {
	this->__viewDir = glm::normalize(position - __position);
	this->__up = glm::normalize(up);
}

float SimpleGL::Camera::farClip() const {
	return __farClip;
}

Camera * SimpleGL::Camera::farClip(float farClip) {
	__farClip = farClip;
	modifyed = true;
	return this;
}

float SimpleGL::Camera::nearClip() const {
	return __nearClip;
}

Camera * SimpleGL::Camera::nearClip(float nearClip) {
	__nearClip = nearClip;
	modifyed = true;
	return this;
}

mat4& SimpleGL::Camera::getViewMatrix() {
	if (modifyed) {
		if (__proj == PROJECTION_PARALLEL) {
			float h = __height / 2;
			float w = h * __aspectRatio;
			viewMatrix = glm::ortho(-w, w, -h, h, __nearClip, __farClip);
		} else {
			viewMatrix = glm::perspective(__horizontalFov, __aspectRatio, __nearClip, __farClip);
		}

		viewMatrix *= glm::lookAt(__position, __position + __viewDir, __up);

		modifyed = false;
	}

	return viewMatrix;
}

void SimpleGL::Camera::translateFilm(vec3 translation) {
	glm::quat q = glm::rotation(__viewDir, vec3(0, 0, -1));

	vec3 rt = glm::rotate(q, translation);

	__position += rt;

	modifyed = true;
}
