#include "mesh/sphere.h"

#include "objects/mesh.h"

using namespace SimpleGL;

Mesh* SimpleGL::SphereGenerator::create() const {
	return create(5, 5, 1.0f);
}

Mesh* SimpleGL::SphereGenerator::create(unsigned parallels, unsigned meridians, float radius) const {
	int numVerts = parallels * meridians + 2;
	unsigned x, y;

	if (parallels < 1 || meridians < 3) return NULL;

	Mesh * mesh = Mesh::create(MESH_DYNAMIC);
	mesh->setNumVertices(numVerts);
	mesh->setElements(NULL, (meridians * 2 * 3) * (parallels));
	mesh->setIndirectEnabled(true);
	mesh->setPrimitiveType(PRIMITIVE_TRIANGLES);

	vec3 * positions = mesh->getPositions();
	unsigned * els = mesh->getElements();

	positions[0] = vec3(0, radius, 0);
	positions[numVerts - 1] = vec3(0, -radius, 0);

	for (y = 0; y < parallels; y++) {
		for (x = 0; x < meridians; x++) {
			float fi = ((float) (y + 1) / (parallels + 1)) * M_PI;
			float theta = ((float) x / meridians) * 2 * M_PI;
			positions[1 + y * meridians + x] = vec3(radius * sin(fi) * cos(theta), radius * cos(fi),
					radius * sin(fi) * sin(theta));
		}
	}

	for (x = 0; x < meridians; x++) {
		for (y = 0; y < parallels - 1; y++) {
			els[(y * meridians + x) * 6] = y * meridians + x + 1;
			els[(y * meridians + x) * 6 + 1] = y * meridians + (x + 1) % meridians + 1;
			els[(y * meridians + x) * 6 + 2] = (y + 1) * meridians + (x + 1) % meridians + 1;
			els[(y * meridians + x) * 6 + 3] = y * meridians + x + 1;
			els[(y * meridians + x) * 6 + 4] = (y + 1) * meridians + (x + 1) % meridians + 1;
			els[(y * meridians + x) * 6 + 5] = (y + 1) * meridians + x + 1;
		}
	}

	els += meridians * (parallels - 1) * 6;

	for (x = 0; x < meridians; x++) {
		*els++ = 0;
		*els++ = 1 + (x + 1) % meridians;
		*els++ = 1 + x;
	}
	for (x = 0; x < meridians; x++) {
		*els++ = numVerts - 1;
		*els++ = 1 + ((y) * meridians) + (x);
		*els++ = 1 + ((y) * meridians) + (x + 1) % meridians;
	}

	mesh->update();

	return mesh;
}
