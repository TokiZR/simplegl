#include "mesh/cube.h"

#include "objects/mesh.h"

using namespace SimpleGL;

Mesh* SimpleGL::CubeGenerator::create() const {
	return create(bounds(vec3(-1), vec3(1)));
}

Mesh* SimpleGL::CubeGenerator::create(bounds bounds) const {
	Mesh * mesh = Mesh::create(MESH_DYNAMIC);
	mesh->setNumVertices(8);
	mesh->setElements(NULL, 24);
	mesh->setIndirectEnabled(true);
	mesh->setPrimitiveType(PRIMITIVE_LINE_LIST);

	vec3 * verts = mesh->getPositions();
	unsigned * edges = mesh->getElements();

	*verts++ = vec3(bounds.min.x, bounds.min.y, bounds.min.z);
	*verts++ = vec3(bounds.min.x, bounds.min.y, bounds.max.z);
	*verts++ = vec3(bounds.min.x, bounds.max.y, bounds.min.z);
	*verts++ = vec3(bounds.min.x, bounds.max.y, bounds.max.z);
	*verts++ = vec3(bounds.max.x, bounds.min.y, bounds.min.z);
	*verts++ = vec3(bounds.max.x, bounds.min.y, bounds.max.z);
	*verts++ = vec3(bounds.max.x, bounds.max.y, bounds.min.z);
	*verts++ = vec3(bounds.max.x, bounds.max.y, bounds.max.z);

	*edges++ = 0;
	*edges++ = 1;

	*edges++ = 0;
	*edges++ = 2;

	*edges++ = 0;
	*edges++ = 4;

	*edges++ = 1;
	*edges++ = 3;

	*edges++ = 1;
	*edges++ = 5;

	*edges++ = 2;
	*edges++ = 3;

	*edges++ = 2;
	*edges++ = 6;

	*edges++ = 3;
	*edges++ = 7;

	*edges++ = 4;
	*edges++ = 5;

	*edges++ = 4;
	*edges++ = 6;

	*edges++ = 5;
	*edges++ = 7;

	*edges++ = 6;
	*edges++ = 7;

	mesh->update();

	return mesh;
}
