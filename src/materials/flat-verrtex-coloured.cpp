#include "materials/flat-vertex-coloured.h"

#include <util/gl.h>
#include <render/renderer.h>
#include <objects/camera.h>
#include <gl/gl-program.h>

using namespace SimpleGL;

static OpenGL::Program * flat_vert_col_program = NULL;

SimpleGL::Materials::FlatVertexColoured::FlatVertexColoured() {
	if (!flat_vert_col_program) {
		flat_vert_col_program = new OpenGL::Program();
		flat_vert_col_program->addShader(Shader::fromFile("shaders/base.vert", SHADER_TYPE_VERTEX));
		flat_vert_col_program->addShader(
				Shader::fromFile("shaders/flat-vertex-coloured.frag", SHADER_TYPE_FRAGMENT));

		bool linked = flat_vert_col_program->linkProgram();
		if (!linked) {
			const string & stats = flat_vert_col_program->getStatus();
			fprintf(stderr, "%s\n", stats.data());
			assert(linked);
		}
	}
}

Program* SimpleGL::Materials::FlatVertexColoured::getProgram() const {
	return flat_vert_col_program;
}

Program* SimpleGL::Materials::FlatVertexColoured::useMaterial(Mesh* mesh, Object* object,
		Scene* scene, SceneRenderer* renderer) const {
	flat_vert_col_program->use();

	return flat_vert_col_program;
}

BlendMode SimpleGL::Materials::FlatVertexColoured::getBlendMode() const {
	return BlendMode::Normal;
}
