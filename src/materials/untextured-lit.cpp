#include "materials/untextured-lit.h"

#include <util/gl.h>
#include <render/renderer.h>
#include <objects/camera.h>
#include <gl/gl-program.h>

using namespace SimpleGL;

static OpenGL::Program * untexturedLit_program = NULL;

/**
 * @{
 * Uniform locations.
 */
static GLint ambientLocation;
static GLint diffuseLocation;
static GLint shineLocation;
static GLint spotLocation;

static GLint cameraPositionLocation;

static GLint activeLightsLocation;
static GLint ambientLightLocation;
static GLint lightPositionsLocation;
static GLint lightColoursLocation;
static GLint lightTypesLocation;
static GLint lightSizesLocation;
/** @} */

SimpleGL::Materials::UntexturedLit::UntexturedLit(colour base) :
		__ambient(base * 0.2f), __diffuese(base * 0.8f), __spot(vec3(0.8, 0.8, 0.8)), __shiness(30) {

	if (!untexturedLit_program) {
		assert(untexturedLit_program->linkProgram());

		untexturedLit_program = new OpenGL::Program();
		untexturedLit_program->addShader(Shader::fromFile("shaders/base.vert", SHADER_TYPE_VERTEX));
		untexturedLit_program->addShader(Shader::fromFile("shaders/untextured-lit.frag", SHADER_TYPE_FRAGMENT));
		diffuseLocation = untexturedLit_program->getUniformLocation("diffuse");
		ambientLocation = untexturedLit_program->getUniformLocation("ambient");
		shineLocation = untexturedLit_program->getUniformLocation("shine");
		spotLocation = untexturedLit_program->getUniformLocation("spot");

		activeLightsLocation = untexturedLit_program->getUniformLocation("activeLights");
		ambientLightLocation = untexturedLit_program->getUniformLocation("ambientLight");
		lightPositionsLocation = untexturedLit_program->getUniformLocation("lightPositions");
		lightColoursLocation = untexturedLit_program->getUniformLocation("lightColours");
		lightTypesLocation = untexturedLit_program->getUniformLocation("lightTypes");
		lightSizesLocation = untexturedLit_program->getUniformLocation("lightSizes");
		cameraPositionLocation = untexturedLit_program->getUniformLocation("cameraPosition");
	}
}

Program* SimpleGL::Materials::UntexturedLit::getProgram() const {
	return untexturedLit_program;
}

Program* SimpleGL::Materials::UntexturedLit::useMaterial(Mesh * mesh, Object * object, Scene* scene, SceneRenderer* renderer) const {
	unsigned i;
	Camera * camera = renderer->camera;

	untexturedLit_program->use();

	/* Lights */
	std::vector<Light *>& lights = scene->lights;
	for (i = 0; i < lights.size() && i < 8; i++) {
		colour& c = lights[i]->col;
		if (lightColoursLocation >= 0) glUniform3f(lightColoursLocation + i, c.r, c.g, c.b);
		if (lightPositionsLocation >= 0) glUniform4fv(lightPositionsLocation + i, 1, glm::value_ptr(lights[i]->position));
		if (lightTypesLocation >= 0) glUniform1i(lightTypesLocation + i, lights[i]->type);
		if (lightSizesLocation >= 0) glUniform1f(lightSizesLocation + i, lights[i]->size);
	}
	glCheck()
	;

	glUniform1i(activeLightsLocation, i);
	glCheck()
	;

	colour& c = scene->ambientLight;
	glUniform3f(ambientLightLocation, c.r, c.g, c.b);
	glCheck()
	;

	/* Materials */
	glUniform3fv(ambientLocation, 1, (float *) glm::value_ptr(__ambient));
	glUniform3fv(diffuseLocation, 1, (float *) glm::value_ptr(__diffuese));
	glUniform3fv(spotLocation, 1, (float *) glm::value_ptr(__spot));
	glUniform1f(shineLocation, __shiness);
	glCheck()
	;

	/* Camera Position */
	vec4 camPos = vec4(camera->position(), camera->projectionType() == PROJECTION_PARALLEL ? 0.0 : 1.0);
	glUniform4fv(cameraPositionLocation, 1, glm::value_ptr(camPos));
	glCheck()
	;

	return untexturedLit_program;
}
