#include "materials/flat.h"

#include <util/gl.h>
#include <render/renderer.h>
#include <objects/camera.h>
#include <gl/gl-program.h>

using namespace SimpleGL;

static OpenGL::Program * flat_program = NULL;

GLint colourLocation;

SimpleGL::Materials::Flat::Flat(colour col) :
		__col(col) {
	if (!flat_program) {
		flat_program = new OpenGL::Program();
		flat_program->addShader(
				Shader::fromFile("shaders/base.vert", SHADER_TYPE_VERTEX));
		flat_program->addShader(
				Shader::fromFile("shaders/flat.frag", SHADER_TYPE_FRAGMENT));

		bool linked = flat_program->linkProgram();
		if (!linked) {
			const string & stats = flat_program->getStatus();
			fprintf(stderr, "%s\n", stats.data());
			assert(linked);
		}

		colourLocation = flat_program->getUniformLocation("colour");
	}
}

Program* SimpleGL::Materials::Flat::getProgram() const {
	return flat_program;
}

Program* SimpleGL::Materials::Flat::useMaterial(Mesh* mesh, Object* object,
		Scene* scene, SceneRenderer* renderer) const {
	flat_program->use();

	glUniform4fv(colourLocation, 1, glm::value_ptr(*(vec4*) &__col));
	glCheckError()
	;

	return flat_program;
}

BlendMode SimpleGL::Materials::Flat::getBlendMode() const {

	if (__col.a >= 0.999f)
		return BlendMode::Normal;
	else
		return BlendMode::Alpha;

}
