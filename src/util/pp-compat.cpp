#include "util/pp-compat.h"

#ifdef PP_COMPAT

using namespace SimpleGL;

Mesh* SimpleGL::fromTriangleMesh(const Graphics::TriangleMesh* tmesh, MeshType type) {
	Mesh * m = Mesh::create(type);

	int i; // We all need eyes
	uint32_t numVertices = tmesh->getData().numberOfVertices;

	m->setNumVertices(numVertices);

	m->setPositions((vec3 *) tmesh->getData().vertices);

	Graphics::TriangleMesh::Triangle * t = tmesh->getData().triangles;
	vec3 * normals = m->getNormals();
	vec3 * mNormals = (vec3 *) tmesh->getData().normals;

	int numTriangles = tmesh->getData().numberOfTriangles;
	m->setElements(NULL, numTriangles * 3);
	m->setPrimitiveType(PRIMITIVE_TRIANGLES);
	unsigned * myTriangles = m->getElements();
	int * mats = m->getMaterials();

	for (i = 0; i < numTriangles; i++, t++) {
		normals[t->v[0]] = mNormals[t->n[0]];
		normals[t->v[1]] = mNormals[t->n[1]];
		normals[t->v[2]] = mNormals[t->n[2]];

		myTriangles[i * 3] = t->v[0];
		myTriangles[i * 3 + 1] = t->v[1];
		myTriangles[i * 3 + 2] = t->v[2];
		mats[i] = t->v[3];
	}

	/* When getting a buffer pointer we must always call update to release it. */
	m->update();

	return m;
}

#endif
