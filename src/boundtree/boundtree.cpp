#include "boundtree/boundtree.h"
#include "objects/object.h"
#include "objects/mesh.h"

#include <algorithm>
#include <stack>
#include <queue>

using namespace SimpleGL;
using namespace std;

#define n_left (nodes[n->left])
#define n_right (nodes[n->right])

#define INFLATION 0.2f

static const int Nil = -1;

class Boundtree::Node {
public:

	/**
	 * Node bounding box.
	 */
	bounds bb;

	int parent;

	union {
		struct {
			/**
			 * Left and right children.
			 */
			int left, right;
		};
		/**
		 * List of child objects.
		 */
		deque<Object *> * leaves;
	};

	/**
	 * How far deep in the tree this node is.
	 */
	unsigned height :15;

	/**
	 * Weather it is a leaf.
	 */
	bool leaf :1;

	Node() {
		parent = Nil;
		left = right = Nil;
		height = 0;
		leaf = true;
	}
};

struct Boundtree::node {
	int index;

	Boundtree * tree;

	node(Boundtree * tree) :
			index(0), tree(tree) {
	}

	node(Boundtree * tree, int idx) :
			index(idx), tree(tree) {
	}

	node & operator=(int index) {
		this->index = index;
		return *this;
	}

	bool operator ==(int i) {
		return index == i;
	}

	bool operator !=(int i) {
		return index != i;
	}

	bool operator ==(node n) {
		return index == n.index;
	}

	bool operator !=(node n) {
		return index != n.index;
	}

	Node * operator->() {
		if (index < (int) tree->nodes.size())
			return &tree->nodes[index];
		else
			return NULL;
	}

	int operator*() {
		return index;
	}

	Node * getNode() {
		if (index < (int) tree->nodes.size())
			return &tree->nodes[index];
		else
			return NULL;
	}

	Node * parent() {
		if (tree->nodes[index].parent != Nil)
			return &tree->nodes[tree->nodes[index].parent];
		else
			return NULL;
	}

	Node * left() {
		if (!tree->nodes[index].leaf)
			return &tree->nodes[tree->nodes[index].left];
		else
			return NULL;
	}

	Node * right() {
		if (!tree->nodes[index].leaf)
			return &tree->nodes[tree->nodes[index].right];
		else
			return NULL;
	}

	bool isValid() {
		return index != Nil;
	}
};

struct Boundtree::Query {
	bounds targ;
	node n;
	unsigned childOffset;

	queue<unsigned> nodeQueue;

	Query(Boundtree * t, bounds search) :
			targ(search), n(t, t->root), childOffset(0) {
	}
};

static pair<bounds, bounds> splitVolume(Object ** olist, unsigned size, unsigned split) {
	bounds lb, hb;
	unsigned i;

	for (i = 0; i < size; i++) {
		if (i < split)
			lb.glob(olist[i]->getExtents().enlarge(INFLATION));
		else
			hb.glob(olist[i]->getExtents().enlarge(INFLATION));
	}

	return pair<bounds, bounds>(lb, hb);
}

static float sumVolume(pair<bounds, bounds> p) {
	return p.first.volume() + p.second.volume();
}

/* leaf nodes are split using the New Linear Algorithm, adapted to 3 dimensions. */
int SimpleGL::Boundtree::splitNode(node n) {
	deque<Object *> & d = *n->leaves;

	Object * inserted = d.back();

	n->leaf = false;
	n->left = *getNode();
	n_left.parent = *n;
	n->right = *getNode();
	n_right.parent = *n;
	n->height++;

	/* The lazy way first */
	if (maxLeaf == 1) {
		n_left.bb = d[0]->getExtents();
		n_left.leaves->push_back(d[0]);
		objects[d[0]] = n->left;

		n_right.bb = d[1]->getExtents();
		n_right.leaves->push_back(d[1]);
		objects[d[1]] = n->right;

		delete &d;

		return n->right;
	}

	unsigned sz = d.size();

	Object * X[sz];
	Object * Y[sz];
	Object * Z[sz];

	unsigned xl, xh, yl, yh, zl, zh, i;

	xl = xh = yl = yh = zl = zh = 0;

	/* Split lists */
	bounds &b = n->bb;
	for (i = 0; i < sz; i++) {
		bounds ob = d[i]->getExtents();
		if (ob.min.x - b.min.x < b.max.x - ob.max.x) {
			X[xl++] = d[i];
		} else {
			X[sz - xh++ - 1] = d[i];
		}

		if (ob.min.y - b.min.y < b.max.y - ob.max.y) {
			Y[yl++] = d[i];
		} else {
			Y[sz - yh++ - 1] = d[i];
		}

		if (ob.min.z - b.min.z < b.max.z - ob.max.z) {
			Z[zl++] = d[i];
		} else {
			Z[sz - zh++ - 1] = d[i];
		}
	}

	assert(xl + xh == sz);
	assert(yl + yh == sz);
	assert(zl + zh == sz);

	delete &d;

	/* split nodes */

	unsigned x, y, z, mn;
	pair<bounds, bounds> bds;

	x = max(xl, xh);
	y = max(yl, yh);
	z = max(zl, zh);
	mn = min( { x, y, z });

	Object ** vec = NULL;
	unsigned split = 0;

	if (x == mn && y != mn && z != mn) {
		vec = X;
		split = xl;
	} else if (x != mn && y == mn && z != mn) {
		vec = Y;
		split = yl;
	} else if (x != mn && y != mn && z == mn) {
		vec = Z;
		split = zl;
	} else {
		float xv, yv, zv;
		pair<bounds, bounds> xbds;
		pair<bounds, bounds> ybds;
		pair<bounds, bounds> zbds;

		xv = sumVolume((xbds = splitVolume(X, sz, xl)));

		yv = sumVolume((ybds = splitVolume(Y, sz, yl)));

		zv = sumVolume((zbds = splitVolume(Z, sz, zl)));

		float minv = min( { xv, yv, zv });

		if (xv == minv && yv != minv && zv != minv) {
			vec = X;
			split = xl;
			bds = xbds;
		} else if (xv != minv && yv == minv && zv != minv) {
			vec = Y;
			split = yl;
			bds = ybds;
		} else {
			vec = Z;
			split = zl;
			bds = zbds;
		}
	}

	assert(vec != NULL);

	if (isnanf(bds.first.max.x)) {
		bds = splitVolume(vec, sz, split);
	}

	int side = -1;

	for (i = 0; i < sz; i++) {
		if (i < split) {
			n_left.leaves->push_back(vec[i]);
			objects[vec[i]] = n->left;
		} else {
			n_right.leaves->push_back(vec[i]);
			objects[vec[i]] = n->right;
		}

		if (vec[i] == inserted) {
			if (i < split)
				side = n->left;
			else
				side = n->right;
		}
	}
	n_left.bb = bds.first;
	n_right.bb = bds.second;

	assert(n_left.leaves->size() + n_right.leaves->size() == sz);
	assert(side != -1);

	return side;
}

Boundtree::node SimpleGL::Boundtree::getNode() {
	node n(this);
	if (nextEmpty >= nodes.size()) {
		nodes.resize(nextEmpty + 1, Node());
	}

	numNodes++;

	n = nextEmpty;
	nextEmpty = n->right == -1 ? nextEmpty + 1 : n->right;

	n->leaves = new deque<Object *>;
	n->leaf = true;
	n->height = 0;

	return n;
}

void SimpleGL::Boundtree::releaseNode(unsigned index) {
	nodes[index].right = nextEmpty;
	nextEmpty = index;

	numNodes--;
}

SimpleGL::Boundtree::Boundtree(unsigned maxDepth, unsigned maximumObjectsPerLeaf) :
		root(0), nodes(*new deque<Node>()), nextEmpty(0), pointCount(0), maxDepth(maxDepth), maxLeaf(
				maximumObjectsPerLeaf), curDepth(0), depth(0), numNodes(0), meshUpToDate(false) {
	treeMesh = Mesh::create(MESH_DYNAMIC);
	treeMesh->setPrimitiveType(PRIMITIVE_LINE_LIST);

	root = *getNode();
}

void SimpleGL::Boundtree::insert(Object* obj) {
	node n(this);
	n = root;

	bounds obound = obj->getExtents().enlarge(INFLATION);

	bool split = false;

	stack<unsigned> st;

	while (42) {
		n->bb.glob(obound);
		if (n->leaf) {
			n->leaves->push_back(obj);
			if (n->leaves->size() > maxLeaf) {
				objects[obj] = splitNode(n);
				split = true;
			} else {
				objects[obj] = *n;
			}
			break;
		} else {
			float lvol = n_left.bb.volume(), rvol = n_right.bb.volume();
			if (n_left.bb.merge(obound).volume() - lvol
					> n_right.bb.merge(obound).volume() - rvol) {
				st.push(*n);
				n = n->right;
			} else {
				st.push(*n);
				n = n->left;
			}
		}
	}

	if (split) {
		while (st.size()) {
			n = st.top();
			n->height = max(n_left.height, n_right.height) + 1;
			st.pop();
		}
	}
}

bool SimpleGL::Boundtree::update(Object* obj) {
	if (!objects.count(obj))
		return false;

	node n(this);

	n = objects[obj];

	bounds obound = obj->getExtents();

	if (n->bb.contains(obound)) {
		return false;
	} else {
		remove(obj);
		insert(obj);
		return true;
	}
}

void SimpleGL::Boundtree::remove(Object* obj) {
	if (!objects.count(obj))
		return;

	node n(this);

	n = objects[obj];

	deque<Object *>::iterator it = find(n->leaves->begin(), n->leaves->end(), obj);

	if (it < n->leaves->end()) {
		assert(n->leaf);
		objects.erase(obj);
		n->leaves->erase(it);
		if (n->leaves->size() == 0 && n != root) {
			/* Join nodes */
			node p(this, n->parent);

			if (n == p->left) {
				releaseNode(*n);
				n = p->right;
			} else {
				releaseNode(*n);
				n = p->left;
			}

			p->bb = n->bb;
			p->leaf = n->leaf;
			if (n->leaf) {
				p->leaves = n->leaves;
				for (Object * op : *n->leaves) {
					objects[op] = *p;
				}
			} else {
				p->left = n->left;
				p.left()->parent = *p;
				p->right = n->right;
				p.right()->parent = *p;
			}
			p->height--;

			releaseNode(*n);
			n = p->parent;
		} else {
			n->bb = bounds();

			for (Object * o : *n->leaves) {
				n->bb.glob(o->getExtents());
			}

			n->bb.enlarge(INFLATION);

			n = n->parent;
		}
	} else {
		assert(!"If the object is on the map it MUST be on the said node.");
		return;
	}

	while (n.isValid()) {
		n->bb = n_left.bb.merge(n_right.bb);
		n->height = max(n_left.height, n_right.height) + 1;
		n = n->parent;
	}
}

Boundtree::Query* SimpleGL::Boundtree::searchPrepare(bounds volume) {
	return new Query(this, volume);
}

unsigned SimpleGL::Boundtree::searchGet(Query* q, Object ** buffer, unsigned bsize) {
	unsigned i, got = 0;
	node n = q->n;
	i = q->childOffset;

	if (!n.isValid())
		return 0;

	while (42) {
		if (n->leaf) {
			for (; i < n->leaves->size() && got < bsize; i++) {
				if (n->leaves->at(i)->getExtents().intersects(q->targ)) {
					buffer[got++] = n->leaves->at(i);
				}
			}
			q->childOffset = 0;
			if (got == bsize) {
				q->n = n;
				q->childOffset = i;
				goto ret;
			}
		} else {
			if (n.left()->bb.intersects(q->targ)) {
				q->nodeQueue.push(n->left);
			}
			if (n.right()->bb.intersects(q->targ)) {
				q->nodeQueue.push(n->right);
			}
		}

		if (q->nodeQueue.size()) {
			n = q->nodeQueue.back();
			q->nodeQueue.pop();
		} else {
			n = -1;
			break;
		}
	}

	ret: ;
	return got;
}

void SimpleGL::Boundtree::searchFinish(Query* q) {
	delete q;
}

static void addBox(bounds & b, vec3 * verts, unsigned vbase, unsigned * edges) {
	*verts++ = vec3(b.min.x, b.min.y, b.min.z);
	*verts++ = vec3(b.min.x, b.min.y, b.max.z);
	*verts++ = vec3(b.min.x, b.max.y, b.min.z);
	*verts++ = vec3(b.min.x, b.max.y, b.max.z);
	*verts++ = vec3(b.max.x, b.min.y, b.min.z);
	*verts++ = vec3(b.max.x, b.min.y, b.max.z);
	*verts++ = vec3(b.max.x, b.max.y, b.min.z);
	*verts++ = vec3(b.max.x, b.max.y, b.max.z);

	*edges++ = vbase + 0;
	*edges++ = vbase + 1;

	*edges++ = vbase + 0;
	*edges++ = vbase + 2;

	*edges++ = vbase + 0;
	*edges++ = vbase + 4;

	*edges++ = vbase + 1;
	*edges++ = vbase + 3;

	*edges++ = vbase + 1;
	*edges++ = vbase + 5;

	*edges++ = vbase + 2;
	*edges++ = vbase + 3;

	*edges++ = vbase + 2;
	*edges++ = vbase + 6;

	*edges++ = vbase + 3;
	*edges++ = vbase + 7;

	*edges++ = vbase + 4;
	*edges++ = vbase + 5;

	*edges++ = vbase + 4;
	*edges++ = vbase + 6;

	*edges++ = vbase + 5;
	*edges++ = vbase + 7;

	*edges++ = vbase + 6;
	*edges++ = vbase + 7;
}

int col(int h, int maxHeight) {
	const int colMap[10] = { 0xFF0000, 0x00FF00, 0x0000FF, 0x00CCFF, 0x00FFFF };
	if (maxHeight <= 4) {
		return colMap[h];
	} else {
		return 0x006666 + ((0x00CCFF - 0x006666) * h) / maxHeight;
	}
}

Mesh* SimpleGL::Boundtree::getMesh() {
	treeMesh->setNumVertices(8 * numNodes);
	treeMesh->setElements(NULL, 24 * numNodes);

	node n(this);
	stack<unsigned> st;

	vec3 * verts = treeMesh->getPositions();
	unsigned * edges = treeMesh->getElements();
	int * mats = treeMesh->getMaterials();

	unsigned base = 0, i;

	st.push(root);

	while (st.size()) {
		n = st.top();
		st.pop();
		//printf("popped %d\n", *n);

		addBox(n->bb, verts + base * 8, base * 8, edges + base * 24);
		for (i = 0; i < 8; i++) {
			*mats++ = col(n->height, nodes[root].height);
		}

		base++;
		if (!n->leaf) {
			if (n->left >= 0) {
				//printf("pushing %d\n", n->left);
				st.push(n->left);
			}
			if (n->right >= 0) {
				//printf("pushing %d\n", n->right);
				st.push(n->right);
			}
		}
	}

	treeMesh->update();

	return treeMesh;
}
