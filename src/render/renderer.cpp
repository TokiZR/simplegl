#include <render/renderer.h>

#include <gl/gl-renderer.h>

using namespace SimpleGL;

SimpleGL::SceneRenderer::SceneRenderer() {
	camera = NULL;
	scene = NULL;
	mode = RENDER_MODE_FILL;
}

SceneRenderer* SimpleGL::SceneRenderer::create() {
	return new GLRenderer();
}
