#include "render/program.h"

#include <gl/gl-program.h>

using namespace SimpleGL;

Program* SimpleGL::Program::create() {
	return new OpenGL::Program();
}
