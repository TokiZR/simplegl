#include "render/shader.h"

#include "gl/gl-shader.h"

#include <util/simple-array.h>

using namespace SimpleGL;

static SimpleArray<char> readFile(const char * path) {
	int cnt;
	SimpleArray<char> arr;
	arr.init();

	char buff[4 * 1024];

	FILE * file = fopen(path, "r");

	if (file == NULL) {
		arr.finalize();
		goto ret;
	}

	while ((cnt = fread(buff, 1, sizeof(buff), file)) > 0) {
		arr.add(buff, cnt);
	}

	ret: ;
	return arr;
}

SimpleGL::Shader::Shader(string name, ShaderType type) :
		name(name), source(NULL), nSources(0), dirty(true), type(type), refCount(0), __ownsSource(false) {
}

Shader* SimpleGL::Shader::create(string name, ShaderType type) throw (ShaderException) {
	return new OpenGL::Shader(name, type);
}

void SimpleGL::Shader::setSource(const char* data) {
	source = singletonSource;
	singletonSource[0] = data;
	nSources = 1;
	dirty = true;
}

bool SimpleGL::Shader::ownsSource() const {
	return __ownsSource;
}

void SimpleGL::Shader::ownsSource(bool ownsSource) {
	__ownsSource = ownsSource;
}

void SimpleGL::Shader::setSource(const char** datav, uint32_t nDatas) {
	source = datav;
	nSources = nDatas;
	dirty = true;
}

Shader* SimpleGL::Shader::fromFile(const char* path, ShaderType type) throw (ShaderException) {
	Shader * s = Shader::create(path, type);

	SimpleArray<char> fData = readFile(path);

	if (!fData.getData()) throw new ShaderException(ShaderException::FILE);

	s->setSource(fData.getData());
	s->ownsSource(true);

	return s;
}

SimpleGL::Shader::~Shader() {
	assert(refCount <= 0);
	if (__ownsSource) {
		int i;
		for (i = 0; i < nSources; i++) {
			free((void *) source[i]);
		}
		if (source && source != singletonSource) free(source);
	}
}
