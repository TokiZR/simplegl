#include "quadtree/quadtree.h"

#include <deque>
#include <queue>
#include <assert.h>

using namespace SimpleGL;
using Query=Quadtree::Query;
using QNode=Quadtree::QNode;

#define point(index) (tree->points[index])

struct Quadtree::Query {
	Quadtree * tree;
	const QNode * node;
	bounds query;
	/**
	 * If we stoiped in the middle of a child node this has the index.
	 */
	unsigned childIndex;

	std::queue<const QNode *> pending;

	Query(Quadtree * tree, bounds query) :
			tree(tree), query(query) {
		node = tree->root;
		childIndex = 0;
		if (!node->nodeBounds.intersects(query)) {
			/* Signals the end of the search. */
			node = NULL;
		}
	}
};

SimpleGL::Quadtree::QNode::QNode(bounds b) :
		nodeBounds(b) {
	pad_leaf_maker = nullptr;
	points.init();
}

SimpleGL::Quadtree::QNode::~QNode() {
	if (isLeaf()) {
		points.finalize();
	} else {
		delete children[0];
		delete children[1];
		delete children[2];
		delete children[3];
	}
}

vec3 SimpleGL::Quadtree::QNode::split(Quadtree* tree) {
	int * pts = points.getData();
	unsigned i, len = points.length();

	vec3 &min = nodeBounds.min, &max = nodeBounds.max;

	vec3 mid = (min + max) / 2.0f;

	vec3 top = vec3(mid.x, max.y, mid.z), left = vec3(min.x, mid.y, mid.z), right = vec3(max.x, mid.y, mid.z), bottom =
			vec3(
					mid.x, min.y, mid.z);

	/* Thge first assignment causes this to become a non-leaf */
	children[0] = new QNode(bounds(mid, max));
	children[1] = new QNode(bounds(left, top));
	children[2] = new QNode(bounds(min, mid));
	children[3] = new QNode(bounds(bottom, right));

	tree->numNodes += 4;

	for (i = 0; i < len; i++) {
		vec3 & p = tree->points[pts[i]];
		if (p.x > mid.x) {
			if (p.y > mid.y) {
				children[0]->insert(tree, pts[i]);
			} else {
				children[3]->insert(tree, pts[i]);
			}
		} else {
			if (p.y > mid.y) {
				children[1]->insert(tree, pts[i]);
			} else {
				children[2]->insert(tree, pts[i]);
			}
		}
	}

	free(pts);

	return mid;
}

void SimpleGL::Quadtree::QNode::insert(Quadtree* tree, int point) {
	tree->curDepth++;

	if (tree->curDepth > tree->depth) tree->depth = tree->curDepth;

	assert(nodeBounds.contains(tree->points[point]));

	if (isLeaf()) {
		if (points.length() >= tree->maximumPointsPerNode && tree->curDepth < tree->maxDepth) {
			vec3 mid = split(tree);
			vec3 & p = tree->points[point];
			if (p.x > mid.x) {
				if (p.y > mid.y) {
					children[0]->insert(tree, point);
				} else {
					children[3]->insert(tree, point);
				}
			} else {
				if (p.y > mid.y) {
					children[1]->insert(tree, point);
				} else {
					children[2]->insert(tree, point);
				}
			}
		} else {
			points.add(point);
		}
	} else {
		vec3 mid = (nodeBounds.min + nodeBounds.max) / 2.0f;
		vec3 & p = tree->points[point];
		if (p.x > mid.x) {
			if (p.y > mid.y) {
				children[0]->insert(tree, point);
			} else {
				children[3]->insert(tree, point);
			}
		} else {
			if (p.y > mid.y) {
				children[1]->insert(tree, point);
			} else {
				children[2]->insert(tree, point);
			}
		}
	}
	tree->curDepth--;
}

SimpleGL::Quadtree::Quadtree(vec3* points, unsigned count, unsigned maxDepth, unsigned maximumPointsPerNode) :
		points(points), pointCount(count), maxDepth(maxDepth), maximumPointsPerNode(maximumPointsPerNode), curDepth(0), depth(
				0), numNodes(1) {
	unsigned i;

	bounds total;
	total.min = total.max = points[0];

	for (i = 1; i < pointCount; i++) {
		total.glob(points[i]);
	}

	root = new QNode(total);

	for (i = 0; i < pointCount; i++) {
		root->insert(this, i);
	}

	treeMesh = Mesh::create(MESH_DYNAMIC);
	/* Using most naive vertex count for simplicity */
	treeMesh->setNumVertices(numNodes * 4);
	treeMesh->setPrimitiveType(PRIMITIVE_LINE_LIST);
	treeMesh->setElements(NULL, numNodes * 8);

	std::queue<const QNode *> queue;
	const QNode * n = root;

	vec3 * pts = treeMesh->getPositions();
	unsigned * lines = treeMesh->getElements();

	int j = 0;

	while (n) {
		/********
		 * Insert a quad corresponding to the bounding box
		 */

		/* Elements/Points */
		const bounds & box = n->getBoundingBox();

		lines[j * 2] = j;
		lines[j * 2 + 1] = j + 1;
		pts[j++] = vec3(box.min.x, box.min.y, 0);

		lines[j * 2] = j;
		lines[j * 2 + 1] = j + 1;
		pts[j++] = vec3(box.min.x, box.max.y, 0);

		lines[j * 2] = j;
		lines[j * 2 + 1] = j + 1;
		pts[j++] = vec3(box.max.x, box.max.y, 0);

		lines[j * 2] = j;
		lines[j * 2 + 1] = j - 3;
		pts[j++] = vec3(box.max.x, box.min.y, 0);

		if (!n->isLeaf()) {
			for (i = 0; i < 4; i++) {
				queue.push(n->getChild(i));
			}
		}

		if (queue.size() > 0) {
			n = queue.front();
			queue.pop();
		} else {
			n = NULL;
		}
	}

	treeMesh->update();
}

SimpleGL::Quadtree::~Quadtree() {
	delete root;
}

Query* SimpleGL::Quadtree::searchPrepare(bounds volume) {
	return new Query(this, volume);
}

unsigned SimpleGL::Quadtree::searchGet(Query* q, int* buffer, unsigned bsize) {
	unsigned got = 0;
	unsigned i;
	const QNode * n = q->node;
	while (n) {
		if (n->isLeaf()) {
			for (i = q->childIndex; i < n->getPoints()->length() && got < bsize; i++) {
				if (q->query.contains(q->tree->points[n->getPoints()->at(i)])) {
					buffer[got++] = n->getPoints()->at(i);
				}
			}
			q->childIndex = 0;
			if (got == bsize) {
				q->node = n;
				q->childIndex = i;
				goto finito;
			}
		} else {
			for (i = 0; i < 4; i++) {
				if (n->getChild(i)->getBoundingBox().intersects(q->query)) {
					q->pending.push(n->getChild(i));
				}
			}
		}
		if (q->pending.size() > 0) {
			n = q->pending.front();
			q->pending.pop();
		} else {
			q->childIndex = 0;
			q->node = NULL;
			break;
		}
	}

	finito: ;
	return got;
}

void SimpleGL::Quadtree::searchFinish(Query* q) {
	delete q;
}
