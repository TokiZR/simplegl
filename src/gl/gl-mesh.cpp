#include "gl/gl-mesh.h"

#include <malloc.h>
#include <string.h>

using namespace SimpleGL;

SimpleGL::GLMesh::GLMesh(MeshType type) :
		Mesh(type) {
	bufferVersion = 0;
	dataVBO = 0;
	elementsVBO = 0;
	meshVAO = 0;

	mappedData = NULL;
	mappedElements = NULL;

	positions = NULL;
	normals = NULL;
	triangles = NULL;
	materials = NULL;

	numElements = numVertices = 0;

	indirectEnabled = true;

	glGenVertexArrays(1, &meshVAO);
	glCheck()
	;
	glBindVertexArray(meshVAO);
	glCheck()
	;
	glGenBuffers(1, &dataVBO);
	glGenBuffers(1, &elementsVBO);
	glCheck()
	;
	glBindVertexArray(0);
}

SimpleGL::GLMesh::~GLMesh() {
	if (dataVBO != 0) glDeleteBuffers(1, &dataVBO);
	if (elementsVBO != 0) glDeleteBuffers(1, &elementsVBO);
	if (meshVAO != 0) glDeleteVertexArrays(1, &meshVAO);
}

void SimpleGL::GLMesh::updateOGLBuffers() {
	glCheck()
	;
	glBindVertexArray(meshVAO);
	glCheck()
	;

	/* Triangles */
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * numElements, triangles, GL_STATIC_DRAW);
	glCheck()
	;

	/* Vertices */
	glBindBuffer(GL_ARRAY_BUFFER, dataVBO);
	glBufferData(GL_ARRAY_BUFFER, (sizeof(vec3) * 2 + sizeof(int)) * numVertices, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec3) * numVertices, positions);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vec3) * numVertices, sizeof(vec3) * numVertices, normals);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vec3) * numVertices * 2, sizeof(int) * numVertices, materials);
	glCheck()
	;

	/* Vertex Array Object */
	glVertexAttribPointer(VERTEX_COORDS_LOCATION, 3, GL_FLOAT, 0, 0, (void *) 0);
	glVertexAttribPointer(VERTEX_NORMAL_LOCATION, 3, GL_FLOAT, 0, 0, (void *) (numVertices * sizeof(vec3)));
	glVertexAttribIPointer(VERTEX_MATERIAL_INDEX_LOCATION, 1, GL_INT, 0, (void *) (numVertices * sizeof(vec3) * 2));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glCheck()
	;
}

MeshBuffers SimpleGL::GLMesh::getBuffers() {
	if (version != bufferVersion) {
		updateOGLBuffers();
		//bufferVersion = version;
	}

	MeshBuffers buffs;
	buffs.dataVAO = meshVAO;
	buffs.elementVBO = elementsVBO;
	buffs.useElements = indirectEnabled;
	return buffs;
}

void SimpleGL::GLMesh::update() {
	if (type == MESH_DYNAMIC) {
		version++;
	} else {
		if (mappedData) {
			glUnmapBuffer(dataVBO);
			mappedData = NULL;
		}
		if (mappedElements) {
			glUnmapBuffer(elementsVBO);
			mappedElements = NULL;
		}

		glBindVertexArray(meshVAO);
		glBindBuffer(GL_ARRAY_BUFFER, dataVBO);
		glCheck()
		;

		glVertexAttribPointer(VERTEX_COORDS_LOCATION, 3, GL_FLOAT, 0, 0, (void *) 0);
		glVertexAttribPointer(VERTEX_NORMAL_LOCATION, 3, GL_FLOAT, 0, 0, (void *) (numVertices * sizeof(vec3)));
		glVertexAttribIPointer(VERTEX_MATERIAL_INDEX_LOCATION, 1, GL_INT, 0, (void *) (numVertices * sizeof(vec3) * 2));
		glCheck()
		;

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glCheck()
		;
	}
}

void SimpleGL::GLMesh::setNumVertices(uint32_t count) {
	if (type == MESH_DYNAMIC) {
		positions = (vec3 *) realloc(positions, sizeof(vec3) * count);
		normals = (vec3 *) realloc(normals, sizeof(vec3) * count);
		materials = (int *) realloc(materials, sizeof(int) * count);
	} else {
		glBindBuffer(GL_ARRAY_BUFFER, dataVBO);
		glBufferData(GL_ARRAY_BUFFER, (sizeof(vec3) * 2 + sizeof(int)) * count, NULL, GL_STATIC_DRAW);
		glCheck()
		;
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	numVertices = count;
}

uint32_t SimpleGL::GLMesh::getNumVertices() {
	return numVertices;
}

unsigned * SimpleGL::GLMesh::getElements() {
	if (type == MESH_DYNAMIC) {
		return triangles;
	} else {
		if (mappedElements == NULL) {
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dataVBO);
			mappedElements = (unsigned *) glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_READ_WRITE);
			glCheck()
			;
		}
		return mappedElements;
	}
}

void SimpleGL::GLMesh::setElements(unsigned * data, uint32_t length) {
	if (type == MESH_DYNAMIC) {
		if (length != numElements) {
			triangles = (unsigned *) realloc(triangles, sizeof(unsigned) * length);
		}
		if (data) memcpy(triangles, data, sizeof(unsigned) * length);
	} else {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsVBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * length, triangles, GL_STATIC_DRAW);
		glCheck()
		;
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	numElements = length;
}

uint32_t SimpleGL::GLMesh::getElementCount() {
	return numElements;
}

vec3* SimpleGL::GLMesh::getPositions() {
	if (type == MESH_DYNAMIC) {
		return positions;
	} else {
		if (mappedData == NULL) {
			glBindBuffer(GL_ARRAY_BUFFER, dataVBO);
			mappedData = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE);
			glCheck()
			;
		}
		return (vec3*) (mappedData);
	}
}

void SimpleGL::GLMesh::setPositions(vec3* data) {
	if (type == MESH_DYNAMIC) {
		memcpy(positions, data, sizeof(vec3) * numVertices);
	} else {
		glBindBuffer(GL_ARRAY_BUFFER, dataVBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vec3) * numVertices, data);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}

vec3* SimpleGL::GLMesh::getNormals() {
	if (type == MESH_DYNAMIC) {
		return normals;
	} else {
		if (mappedData == NULL) {
			glBindBuffer(GL_ARRAY_BUFFER, dataVBO);
			mappedData = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE);
			glCheck()
			;
		}
		/* Get the normals offset */
		return (vec3*) (mappedData + (sizeof(vec3) * numVertices));
	}
}

void SimpleGL::GLMesh::setNormals(vec3* data) {
	if (type == MESH_DYNAMIC) {
		memcpy(normals, data, sizeof(vec3) * numVertices);
	} else {
		glBindBuffer(GL_ARRAY_BUFFER, dataVBO);
		glBufferSubData(GL_ARRAY_BUFFER, sizeof(vec3) * numVertices, sizeof(vec3) * numVertices, data);
		glCheck()
		;
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}

int* SimpleGL::GLMesh::getMaterials() {
	if (type == MESH_DYNAMIC) {
		return materials;
	} else {
		if (mappedData == NULL) {
			glBindBuffer(GL_ARRAY_BUFFER, dataVBO);
			mappedData = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE);
			glCheck()
			;
		}
		/* Get the normals offset */
		return (int*) (mappedData + (sizeof(vec3) * numVertices * 2));
	}
}

void SimpleGL::GLMesh::setIndirectEnabled(bool indirect) {
	indirectEnabled = indirect;
}

void SimpleGL::GLMesh::setMaterials(int* data) {
	if (type == MESH_DYNAMIC) {
		memcpy(materials, data, sizeof(int) * numVertices);
	} else {
		glBindBuffer(GL_ARRAY_BUFFER, dataVBO);
		glBufferSubData(GL_ARRAY_BUFFER, sizeof(vec3) * numVertices * 2, sizeof(int) * numVertices, data);
		glCheck()
		;
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}

bounds SimpleGL::GLMesh::getBounds() const {
	if (!numVertices) return bounds();

	unsigned i;
	vec3 * p = NULL;

	if (type == MESH_DYNAMIC) {
		p = positions;
	} else {
		if (mappedData == NULL) {
			glBindBuffer(GL_ARRAY_BUFFER, dataVBO);
			p = (vec3*) glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE);
			glCheck()
			;
		}
	}

	if (p == NULL) {
		// TODO : Add logging
	}

	bounds b(p[0], p[0]);

	for (i = 0; i < numVertices; i++) {
		b.glob(p[i]);
	}

	return b;
}
