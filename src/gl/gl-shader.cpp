#include <gl/gl-shader.h>

#include <util/gl.h>

using namespace SimpleGL;

SimpleGL::OpenGL::Shader::Shader(string name, ShaderType tp) throw (ShaderException) :
		SimpleGL::Shader(name, tp) {
	int gltype = 0;

	switch (tp) {
		case SHADER_TYPE_VERTEX:
			gltype = GL_VERTEX_SHADER;
			break;
		case SHADER_TYPE_GEOMETRY:
			gltype = GL_GEOMETRY_SHADER;
			break;
		case SHADER_TYPE_TESSELATION_CONTROL:
			gltype = GL_TESS_CONTROL_SHADER;
			break;
		case SHADER_TYPE_TESSELATION_EVALUATION:
			gltype = GL_TESS_EVALUATION_SHADER;
			break;
		case SHADER_TYPE_FRAGMENT:
			gltype = GL_FRAGMENT_SHADER;
			break;
	}

	handle = glCreateShader(gltype);
	glCheckError();
	if (!handle) {
		int error = glGetError();
		if (error == GL_OUT_OF_MEMORY) {
			throw(ShaderException(ShaderException::NO_MEMORY));
		} else if (error == GL_INVALID_ENUM) {
			throw(ShaderException(ShaderException::UNSUPORTED));
		}
	}
}

SimpleGL::OpenGL::Shader::~Shader() {
	glDeleteProgram(handle);
}

bool SimpleGL::OpenGL::Shader::compile() {
	if (nSources == 0) {
		return false;
	} else {
		glShaderSource(handle, nSources, source, NULL);
		glCheckError();
	}

	glCompileShader(handle);
	glCheckError();

	int success;
	glGetShaderiv(handle, GL_COMPILE_STATUS, &success);
	glCheckError();

	return success != GL_FALSE;
}

string SimpleGL::OpenGL::Shader::getStatus() const {
	int len;
	glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &len);

	string log;
	log.resize(len, '\0');
	glGetShaderInfoLog(handle, len, &len, &log[0]);
	log.resize(len);

	return log;
}
