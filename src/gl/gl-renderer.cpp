#include <gl/gl-renderer.h>

#include <gl/gl-mesh.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <objects/material.h>

#include <GL/freeglut.h>

using namespace SimpleGL;

int sglPrimitive2gl(PrimitiveType tp) {
	switch (tp) {
		case PRIMITIVE_POINTS:
			return GL_POINTS;
		case PRIMITIVE_LINE_LIST:
			return GL_LINES;
		case PRIMITIVE_LINE_STRIP:
			return GL_LINE_STRIP;
		case PRIMITIVE_LINE_LOOP:
			return GL_LINE_LOOP;
		case PRIMITIVE_TRIANGLES:
			return GL_TRIANGLES;
		case PRIMITIVE_TRIANGLE_STRIP:
			return GL_TRIANGLE_STRIP;
		case PRIMITIVE_TRIANGLE_FAN:
			return GL_TRIANGLE_FAN;
	}

	return -1;
}

/**
 * Cast anything that kinda looks like a vec3 into a glm::vec3.
 */
#define glmVec(vec) (* (glm::vec3 *) (&vec))

SimpleGL::GLRenderer::GLRenderer() {

	cameraVersion = 0;
}

SimpleGL::GLRenderer::~GLRenderer() {
}

void SimpleGL::GLRenderer::renderScene() {
	//printf("Drawing\n");

	glCheck()
	;

	/**
	 * Prepare
	 */
	const colour& bg = scene->background;

	glClearColor((float) bg.r, (float) bg.g, (float) bg.b, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (mode == RENDER_MODE_FILL) {
		glPolygonMode(GL_FRONT, GL_FILL);
	} else {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	glEnable(GL_MULTISAMPLE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCheck()
	;

	checkView();

	/**
	 * Locations of the basic uniforms on any program.
	 *
	 */
	GLint mvpLocation;
	GLint inverseTransformLocation;

	OpenGL::Program * program, *oldProgram = NULL;

	/**
	 * Set scene wide uniforms.
	 */

	;
	/**
	 * Traverse scene
	 */

	/* Oi!? */
	ObjectIterator oi(scene->root);

	if (!oi.isVisible()) goto skip;

	/* That should take a while */
	while (42) {
		/* Prepare the transform. */
		glm::mat4 transf(1.0);

		transf = glm::translate(transf, glmVec(oi->position));
		transf = glm::rotate(transf, oi->rotation.z, glm::vec3(0.0, 0.0, 1.0));
		transf = glm::rotate(transf, oi->rotation.y, glm::vec3(0.0, 1.0, 0.0));
		transf = glm::rotate(transf, oi->rotation.x, glm::vec3(1.0, 0.0, 0.0));
		transf = glm::scale(transf, glmVec(oi->scale));

		if (transformStack.size()) transf = transformStack.back() * transf;

		/* Draw Any meshes */
		for (Object::MeshRef & mr : oi->meshes) {
			Mesh * m = mr.mesh;
			MeshBuffers buffers = static_cast<GLMesh *>(m)->getBuffers();
			Material * mat = oi->materials[mr.matIndex];

			program = static_cast<OpenGL::Program *>(mat->useMaterial(m, *oi, scene, this));

			if (mat->getBlendMode() != BlendMode::Normal) {
				glEnable(GL_BLEND);
				glDisable(GL_DEPTH_TEST);
				switch (mat->getBlendMode()) {
					case BlendMode::Add:
						glBlendFunc(GL_ONE, GL_ONE);
						break;
					case BlendMode::Subtract:
						glBlendFunc(GL_ONE, GL_ONE);

						glBlendEquation(GL_FUNC_SUBTRACT);
						glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
						break;
					case BlendMode::Alpha:
						glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
						break;
					case BlendMode::PremultipliedAlpha:
						glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
						break;
					default:
						break;
				}
			}

			mvpLocation = program->getUniformLocation("mvpMatrix");
			inverseTransformLocation = program->getUniformLocation("inverseTransformMatrix");

			// TODO : this kinda causes some ignorance on the transforms of objects.
			if (true){//program != oldProgram) {
				glm::mat4 mvp = mvpMatrix * transf;
				glUniformMatrix4fv(mvpLocation, 1, 0, glm::value_ptr(mvp));
				glCheck()
				;

				/* Set the uniform */
				glm::mat4 invTranspModel = glm::inverseTranspose(transf);
				glUniformMatrix4fv(inverseTransformLocation, 1, 0, glm::value_ptr(invTranspModel));
			} else {
				oldProgram = program;
			}

			glBindVertexArray(buffers.dataVAO);
			glCheck()
			;

			glEnableVertexAttribArray(VERTEX_COORDS_LOCATION);
			glEnableVertexAttribArray(VERTEX_NORMAL_LOCATION);
			glEnableVertexAttribArray(VERTEX_MATERIAL_INDEX_LOCATION);
			glCheck()
			;

			if (buffers.useElements) {
				glBindBuffer(GL_ARRAY_BUFFER, buffers.elementVBO);
				glDrawElements(sglPrimitive2gl(m->getPrimitiveType()), m->getElementCount(),
				GL_UNSIGNED_INT, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
			} else {
				glDrawArrays(sglPrimitive2gl(m->getPrimitiveType()), 0, m->getNumVertices());
			}
			glCheck()
			;
			glBindVertexArray(0);

			if (mat->getBlendMode() != BlendMode::Normal) {
				glEnable(GL_DEPTH_TEST);
				glDisable(GL_BLEND);
			}
		}

		/* Go on to the children, siblings or 'uncle' nodes. */
		if (oi.firstVisibleChild()) {
			transformStack.push_back(transf);
			oi = oi.firstVisibleChild();
		} else if (oi.nextVisible()) {
			oi = oi.nextVisible();
		} else {
			/* Try to find another object higher in the tree with a sibling */
			tryUp: ;
			oi = oi.parent();
			if (oi.isValid()) {
				transformStack.pop_back();
				if (oi.nextVisible()) {
					oi = oi.nextVisible();
				} else {
					goto tryUp;
				}
			} else {
				/* No parent, we are done. */
				break;
			}
		}
	}

	/**
	 * Finish
	 */
	skip: ;
	//glFlush();
	//glutSwapBuffers();
}

void SimpleGL::GLRenderer::setViewport(vec2 dimensions) {
	dims = dimensions;
	/* The lazy solution */
	cameraVersion = -1;
	glViewport(0, 0, dims.x, dims.y);
	glutPostRedisplay();
}

void SimpleGL::GLRenderer::checkView() {
	mvpMatrix = camera->getViewMatrix();
	glViewport(0, 0, dims.x, dims.y);
}

vec2 SimpleGL::GLRenderer::getViewport() {
	return dims;
}
