#include <util/gl.h>

#include <gl/gl-program.h>
#include <gl/gl-shader.h>
#include <gl/opengl.h>

using namespace SimpleGL;

SimpleGL::OpenGL::Program::Program() :
		pendingRelink(false), linked(false) {
	handle = glCreateProgram();
	glCheckError()
	;
}

SimpleGL::OpenGL::Program::~Program() {
	glDeleteProgram(handle);
}

bool SimpleGL::OpenGL::Program::linkProgram() {
	if (!pendingRelink && linked) return true;
	string s;

	status.clear();

	int i;
	for (i = 0; i < SHADER_NUM_SHADER_TYPES; i++) {
		if (stages[i] && !stages[i]->compile()) {
			status += stages[i]->getName();
			status += "\n";
			status += stages[i]->getStatus();
			//status += "\n";
		}
	}

	glLinkProgram(handle);
	glCheckError()
	;

	int linkStatus;
	glGetProgramiv(handle, GL_LINK_STATUS, &linkStatus);
	glCheckError()
	;
	linked = linkStatus;
	pendingRelink = false;

	if (linked) {
		int cnt;
		glGetProgramiv(handle, GL_ACTIVE_UNIFORMS, &cnt);

		if (cnt <= 0) goto end;

		unsigned i, type;
		int len;
		int arrayLen;
		for (i = 0; i < (unsigned) cnt; i++) {
			uniforms.emplace_back();
			UniformInfo& ui = uniforms.back();

			glGetActiveUniformsiv(handle, 1, &i, GL_UNIFORM_NAME_LENGTH, &len);
			glCheckError()
			;

			ui.name.resize(len);
			glGetActiveUniform(handle, i, len, &len, &arrayLen, &type,
					&ui.name[0]);
			//glGetActiveUniformName(handle, i, len, &len, &ui.name[0]);
			ui.name.resize(len);
			glCheckError()
			;

			//glGetActiveUniformsiv(handle, 1, &i, GL_UNIFORM_TYPE, &type);
			//glCheckError()
			;
			ui.type = glTypeToHades(type);

			//glGetActiveUniformsiv(handle, 1, &i, GL_UNIFORM_SIZE, &arrayLen);
			//glCheckError()
			;
			ui.arrayLength = arrayLen;
			ui.array = arrayLen > 0;

			ui.location = glGetUniformLocation(handle, ui.name.data());
			glCheckError()
			;

			//printf("%48s %4d %5d\n", uniformName, size, type);
		}
	} else {
		uniforms.clear();
	}

	int len;
	glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &len);

	s.resize(len, '\0');

	glGetProgramInfoLog(handle, len, &len, &s[0]);
	s.resize(len, '\0');

	status += s;

	end: ;
	return linkStatus == GL_TRUE;
}

const std::string& SimpleGL::OpenGL::Program::getStatus() {
	return status;
}

void SimpleGL::OpenGL::Program::addShader(SimpleGL::Shader* hshader) {
	pendingRelink = true;

	/* Should not fail, in order to mock this up the user must do it deliberately. */
	assert(dynamic_cast<Shader *>(hshader) != NULL);

	Shader * shader = static_cast<Shader *>(hshader);

	/* Remove old shader if any */
	Shader * oldShader = static_cast<Shader *>(stages[shader->getType()]);
	if (oldShader) {
		glDetachShader(handle, oldShader->getHandle());
		oldShader->unref();
	}

	/* Add new shader if not null */
	stages[shader->getType()] = shader;
	if (shader) {
		glAttachShader(handle, shader->getHandle());
		shader->ref();
	}
}

void SimpleGL::OpenGL::Program::removeShader(SimpleGL::Shader* hshader) {
	pendingRelink = true;

	assert(dynamic_cast<Shader *>(hshader) != NULL);

	Shader * shader = static_cast<Shader *>(hshader);

	/* check if have shader */
	Shader * oldShader = static_cast<Shader *>(stages[shader->getType()]);
	if (oldShader && oldShader == shader) {
		glDetachShader(handle, oldShader->getHandle());
		oldShader->unref();
	}
}

SimpleGL::Shader* SimpleGL::OpenGL::Program::removeShader(ShaderType type,
		bool unref) {
	pendingRelink = true;

	/* check if any */
	Shader * shader = static_cast<Shader *>(stages[type]);
	if (shader) {
		glDetachShader(handle, shader->getHandle());
		if (unref) {
			/* Set to null if destroyed */
			shader = shader->unref() ? NULL : shader;
		}
	}

	return shader;
}

SimpleGL::Shader* SimpleGL::OpenGL::Program::getShader(ShaderType tp) {
	return stages[tp];
}

int SimpleGL::OpenGL::Program::getAttributeLocation(
		const char* attrName) const {
	return glGetAttribLocation(handle, attrName);
}

const std::list<UniformInfo>& SimpleGL::OpenGL::Program::getUniforms() {
	return uniforms;
}

const UniformInfo* SimpleGL::OpenGL::Program::getUniformInfo(const char* name) {
	for (UniformInfo& info : uniforms) {
		if (strcmp(info.name.data(), name) == 0) {
			return &info;
		}
	}

	return NULL;
}

void SimpleGL::OpenGL::Program::setUniform(ShadingLanguageType utp,
		int uniformLoc, void* data, int count) {
	switch (utp) {
		default:
		case SLANG_TYPE_INVALID:
			break;
		case SLANG_TYPE_FLOAT:
			glUniform1fv(uniformLoc, count, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_VEC2:
			glUniform2fv(uniformLoc, count, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_VEC3:
			glUniform3fv(uniformLoc, count, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_VEC4:
			glUniform4fv(uniformLoc, count, (float *) data);
			break;
		case SLANG_TYPE_DOUBLE:
			glUniform1dv(uniformLoc, count, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_VEC2:
			glUniform2dv(uniformLoc, count, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_VEC3:
			glUniform3dv(uniformLoc, count, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_VEC4:
			glUniform4dv(uniformLoc, count, (double *) data);
			break;
		case SLANG_TYPE_INT:
			glUniform1iv(uniformLoc, count, (int *) data);
			break;
		case SLANG_TYPE_INT_VEC2:
			glUniform2iv(uniformLoc, count, (int *) data);
			break;
		case SLANG_TYPE_INT_VEC3:
			glUniform3iv(uniformLoc, count, (int *) data);
			break;
		case SLANG_TYPE_INT_VEC4:
			glUniform4iv(uniformLoc, count, (int *) data);
			break;
		case SLANG_TYPE_UNSIGNED_INT:
			glUniform1uiv(uniformLoc, count, (unsigned *) data);
			break;
		case SLANG_TYPE_UNSIGNED_INT_VEC2:
			glUniform2uiv(uniformLoc, count, (unsigned *) data);
			break;
		case SLANG_TYPE_UNSIGNED_INT_VEC3:
			glUniform3uiv(uniformLoc, count, (unsigned *) data);
			break;
		case SLANG_TYPE_UNSIGNED_INT_VEC4:
			glUniform4uiv(uniformLoc, count, (unsigned *) data);
			break;
		case SLANG_TYPE_BOOL:
			glUniform1uiv(uniformLoc, count, (unsigned *) data);
			break;
		case SLANG_TYPE_BOOL_VEC2:
			glUniform2uiv(uniformLoc, count, (unsigned *) data);
			break;
		case SLANG_TYPE_BOOL_VEC3:
			glUniform3uiv(uniformLoc, count, (unsigned *) data);
			break;
		case SLANG_TYPE_BOOL_VEC4:
			glUniform4uiv(uniformLoc, count, (unsigned *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT2:
			glUniformMatrix2fv(uniformLoc, count, GL_FALSE, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT3:
			glUniformMatrix3fv(uniformLoc, count, GL_FALSE, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT4:
			glUniformMatrix4fv(uniformLoc, count, GL_FALSE, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT2x3:
			glUniformMatrix2x3fv(uniformLoc, count, GL_FALSE, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT2x4:
			glUniformMatrix2x4fv(uniformLoc, count, GL_FALSE, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT3x2:
			glUniformMatrix3x2fv(uniformLoc, count, GL_FALSE, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT3x4:
			glUniformMatrix3x4fv(uniformLoc, count, GL_FALSE, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT4x2:
			glUniformMatrix4x2fv(uniformLoc, count, GL_FALSE, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT4x3:
			glUniformMatrix4x3fv(uniformLoc, count, GL_FALSE, (float *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT2:
			glUniformMatrix2dv(uniformLoc, count, GL_FALSE, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT3:
			glUniformMatrix3dv(uniformLoc, count, GL_FALSE, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT4:
			glUniformMatrix4dv(uniformLoc, count, GL_FALSE, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT2x3:
			glUniformMatrix2x3dv(uniformLoc, count, GL_FALSE, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT2x4:
			glUniformMatrix2x4dv(uniformLoc, count, GL_FALSE, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT3x2:
			glUniformMatrix3x2dv(uniformLoc, count, GL_FALSE, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT3x4:
			glUniformMatrix3x4dv(uniformLoc, count, GL_FALSE, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT4x2:
			glUniformMatrix4x2dv(uniformLoc, count, GL_FALSE, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT4x3:
			glUniformMatrix4x3dv(uniformLoc, count, GL_FALSE, (double *) data);
			break;
		case SLANG_TYPE_SAMPLER_1D:
		case SLANG_TYPE_SAMPLER_2D:
		case SLANG_TYPE_SAMPLER_3D:
		case SLANG_TYPE_SAMPLER_CUBE:
		case SLANG_TYPE_SAMPLER_1D_SHADOW:
		case SLANG_TYPE_SAMPLER_2D_SHADOW:
		case SLANG_TYPE_SAMPLER_1D_ARRAY:
		case SLANG_TYPE_SAMPLER_2D_ARRAY:
		case SLANG_TYPE_SAMPLER_1D_ARRAY_SHADOW:
		case SLANG_TYPE_SAMPLER_2D_ARRAY_SHADOW:
		case SLANG_TYPE_SAMPLER_2D_MULTISAMPLE:
		case SLANG_TYPE_SAMPLER_2D_MULTISAMPLE_ARRAY:
		case SLANG_TYPE_SAMPLER_CUBE_SHADOW:
		case SLANG_TYPE_SAMPLER_BUFFER:
		case SLANG_TYPE_SAMPLER_2D_RECT:
		case SLANG_TYPE_SAMPLER_2D_RECT_SHADOW:
		case SLANG_TYPE_INT_SAMPLER_1D:
		case SLANG_TYPE_INT_SAMPLER_2D:
		case SLANG_TYPE_INT_SAMPLER_3D:
		case SLANG_TYPE_INT_SAMPLER_CUBE:
		case SLANG_TYPE_INT_SAMPLER_1D_ARRAY:
		case SLANG_TYPE_INT_SAMPLER_2D_ARRAY:
		case SLANG_TYPE_INT_SAMPLER_2D_MULTISAMPLE:
		case SLANG_TYPE_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
		case SLANG_TYPE_INT_SAMPLER_BUFFER:
		case SLANG_TYPE_INT_SAMPLER_2D_RECT:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_1D:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_3D:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_CUBE:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_1D_ARRAY:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D_ARRAY:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_BUFFER:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D_RECT:
			glUniform1iv(uniformLoc, count, (int *) data);
			break;
	}
}

void SimpleGL::OpenGL::Program::getUniform(ShadingLanguageType utp,
		int uniformLoc, void* data, int count) {
	switch (utp) {
		default:
		case SLANG_TYPE_INVALID:
			break;
		case SLANG_TYPE_FLOAT:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_VEC2:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_VEC3:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_VEC4:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_DOUBLE:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_VEC2:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_VEC3:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_VEC4:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_INT:
			glGetUniformiv(handle, uniformLoc, (int *) data);
			break;
		case SLANG_TYPE_INT_VEC2:
			glGetUniformiv(handle, uniformLoc, (int *) data);
			break;
		case SLANG_TYPE_INT_VEC3:
			glGetUniformiv(handle, uniformLoc, (int *) data);
			break;
		case SLANG_TYPE_INT_VEC4:
			glGetUniformiv(handle, uniformLoc, (int *) data);
			break;
		case SLANG_TYPE_UNSIGNED_INT:
			glGetUniformuiv(handle, uniformLoc, (unsigned *) data);
			break;
		case SLANG_TYPE_UNSIGNED_INT_VEC2:
			glGetUniformuiv(handle, uniformLoc, (unsigned *) data);
			break;
		case SLANG_TYPE_UNSIGNED_INT_VEC3:
			glGetUniformuiv(handle, uniformLoc, (unsigned *) data);
			break;
		case SLANG_TYPE_UNSIGNED_INT_VEC4:
			glGetUniformuiv(handle, uniformLoc, (unsigned *) data);
			break;
		case SLANG_TYPE_BOOL:
			glGetUniformuiv(handle, uniformLoc, (unsigned *) data);
			break;
		case SLANG_TYPE_BOOL_VEC2:
			glGetUniformuiv(handle, uniformLoc, (unsigned *) data);
			break;
		case SLANG_TYPE_BOOL_VEC3:
			glGetUniformuiv(handle, uniformLoc, (unsigned *) data);
			break;
		case SLANG_TYPE_BOOL_VEC4:
			glGetUniformuiv(handle, uniformLoc, (unsigned *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT2:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT3:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT4:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT2x3:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT2x4:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT3x2:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT3x4:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT4x2:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_FLOAT_MAT4x3:
			glGetUniformfv(handle, uniformLoc, (float *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT2:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT3:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT4:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT2x3:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT2x4:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT3x2:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT3x4:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT4x2:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_DOUBLE_MAT4x3:
			glGetUniformdv(handle, uniformLoc, (double *) data);
			break;
		case SLANG_TYPE_SAMPLER_1D:
		case SLANG_TYPE_SAMPLER_2D:
		case SLANG_TYPE_SAMPLER_3D:
		case SLANG_TYPE_SAMPLER_CUBE:
		case SLANG_TYPE_SAMPLER_1D_SHADOW:
		case SLANG_TYPE_SAMPLER_2D_SHADOW:
		case SLANG_TYPE_SAMPLER_1D_ARRAY:
		case SLANG_TYPE_SAMPLER_2D_ARRAY:
		case SLANG_TYPE_SAMPLER_1D_ARRAY_SHADOW:
		case SLANG_TYPE_SAMPLER_2D_ARRAY_SHADOW:
		case SLANG_TYPE_SAMPLER_2D_MULTISAMPLE:
		case SLANG_TYPE_SAMPLER_2D_MULTISAMPLE_ARRAY:
		case SLANG_TYPE_SAMPLER_CUBE_SHADOW:
		case SLANG_TYPE_SAMPLER_BUFFER:
		case SLANG_TYPE_SAMPLER_2D_RECT:
		case SLANG_TYPE_SAMPLER_2D_RECT_SHADOW:
		case SLANG_TYPE_INT_SAMPLER_1D:
		case SLANG_TYPE_INT_SAMPLER_2D:
		case SLANG_TYPE_INT_SAMPLER_3D:
		case SLANG_TYPE_INT_SAMPLER_CUBE:
		case SLANG_TYPE_INT_SAMPLER_1D_ARRAY:
		case SLANG_TYPE_INT_SAMPLER_2D_ARRAY:
		case SLANG_TYPE_INT_SAMPLER_2D_MULTISAMPLE:
		case SLANG_TYPE_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
		case SLANG_TYPE_INT_SAMPLER_BUFFER:
		case SLANG_TYPE_INT_SAMPLER_2D_RECT:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_1D:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_3D:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_CUBE:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_1D_ARRAY:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D_ARRAY:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_BUFFER:
		case SLANG_TYPE_UNSIGNED_INT_SAMPLER_2D_RECT:
			glGetUniformiv(handle, uniformLoc, (int *) data);
			break;
	}
}

void SimpleGL::OpenGL::Program::use() {
	if (this->pendingRelink) linkProgram();
	glUseProgram(handle);
	glCheckError()
	;
// TODO : Add logging
}

int SimpleGL::OpenGL::Program::getUniformLocation(const char* name) {
	return glGetUniformLocation(handle, name);
}
