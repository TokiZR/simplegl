# SimpleGL

Simple opengl rendering pipeline with demos.

## Quadtree
 
Demo of a quadtree data-structure to speed up searches on a static point cloud.


## Collision

Demo of an BVH for accelerating collision detection. This tree speeded up with n-ary leaf nodes (using the NLA insertion algorithm from R-Trees) which is faster than the binary variant. The tree also features node caching to reduce allocation times.
