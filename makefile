################################################################################
#
# This is a simple general purpose makefile for managing small C projects.
# It attempts to ensure that your files stay organised by placing dependency
# files, object files, and any other form of output in separate directories.
# This can be further extended to manage any other project 
#

################################################################################
# 
# Configurables.
#

# Argument to pass to the compiler to prompt it to generate dependencies.
CDEP_FLAGS := -MM

# Directories
# Source directory
SRC := src
# Where headers are
INCLUDE := include
# Directory to store dependency files
DEPEND := build/dep
# Intermediate binaries directory
BIN := build/bin
# Built shared objects directory
LIB := lib

#Libs to be linked
LIBS = glew gl glu

LDFLAGS += $(shell pkg-config --libs $(LIBS)) -lglut
INCLUDES += -I$(INCLUDE) -Isrc $(shell pkg-config --cflags $(LIBS))
GCC_MISC += -pthread -std=gnu++0x -D_GNU_SOURCE -D_GLIBCXX_DEBUG -O3 -flto
WARNINGS += -Wall -Wno-pointer-arith

# Files to be built relative to the source folder.
# Categorized on a per folder basis so it doesn't suck as bad

FILES = $(shell find src/ -name '*.cpp' | sed 's/src\///' | grep -v 'pp/')

# Final binary name
MAIN := gldemo

# Compiler paths
export CC = g++
export LD = g++

################################################################################
#
# Rules etc.
#

objects := $(addprefix $(BIN)/,$(FILES:.cpp=.o))
deps := $(addprefix $(DEPEND)/,$(FILES:.cpp=.d))

# Main rule
.PHONY : all
all : quadtree collision

# Include dependency rules. Auto-generated during build
-include $(deps)

# C rules, create new ones for other languages
$(objects): $(BIN)/%.o: $(SRC)/%.cpp
	@mkdir -p $(@D)
	@mkdir -p $(DEPEND)/$(dir $*)
	$(CC) -c -o $@ $(CFLAGS) $(INCLUDES) $(GCC_MISC) $(WARNINGS) -fPIC $<
	@echo -n $(@D)/ > $(DEPEND)/$*.d
	@$(CC) $(CFLAGS) $(INCLUDES) $(GCC_MISC) $(WARNINGS) $(CDEP_FLAGS) >> $(DEPEND)/$*.d $<

# Final binary
quadtree : $(objects) apps/quadtree.cpp
	$(LD) -o quadtree apps/quadtree.cpp $(objects) $(LDFLAGS) $(INCLUDES) $(GCC_MISC) $(WARNINGS)

collision : $(objects) apps/collision.cpp
	$(LD) -o collision apps/collision.cpp $(objects) $(LDFLAGS) $(INCLUDES) $(GCC_MISC) $(WARNINGS)

# Clean
.PHONY : clean
clean :
	rm -rf quadtree collision $(BIN) $(LIB) $(DEPEND) build




