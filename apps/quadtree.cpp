#include<util/gl.h>

#include <objects/scene.h>
#include <render/renderer.h>
#include <quadtree/quadtree.h>
#include <mesh/sphere.h>

#include <materials/flat.h>
#include <materials/flat-vertex-coloured.h>
#include <materials/untextured-lit.h>

#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <random>
#include <algorithm>
#include <deque>

using namespace SimpleGL;

/**
 * Constants
 */
#define POINT_TAKEN 0x00FFFF
#define POINT_CLEAR 0xFFFF00

/* Camera */
const float CAMERA_RES = 0.01f;
const float ZOOM_SCALE = 1.05f;

/**
 * Scene
 */
SceneRenderer * renderer;
Scene * scene;
bool draggingScene = false;
double pointSize = 0.1;

/* Window size */
int windowWidth = 800;
int windowHeight = 600;

/* Mouse */
int mouseX, mouseY;

/* Keyboard */
const int MAX_KEYS = 256;
bool keys[MAX_KEYS];

/* Camera */
float cameraDistance = 5;

/**
 * Probing
 */
unsigned numProbes = 10;
Object ** probeSpheres;
std::deque<int> * probeResults;
Mesh * testMesh;

/* Probe spheres */
float sphereRadius = 1.0;
float sphererRadiusSquared = sphereRadius * sphereRadius;
int draggedProbe = -1;

/* Quadtree */
Quadtree * qtree;
Object * qtObject;
Mesh * pointCloud;

/**
 * Update the highlighted points for the new query.
 */
void updateProbeSphere(unsigned index) {
	int indices[256];
	int cnt, i;

	int * mats = pointCloud->getMaterials();
	vec3 * positions = pointCloud->getPositions();

	vec3 & center = probeSpheres[index]->position;
	bounds bbox(center - vec3(sphereRadius, sphereRadius, 0), center + vec3(sphereRadius, sphereRadius, 0));

	vec3 * pt = testMesh->getPositions();
	pt[index] = center;

	for (i = 0; i < (int) probeResults[index].size(); i++) {
		mats[probeResults[index][i]] = POINT_CLEAR;
	}

	probeResults[index].clear();

	Quadtree::Query * query = qtree->searchPrepare(bbox);
	do {
		/* Rough search */
		cnt = qtree->searchGet(query, indices, 256);

		/* Refine */
		int offset = 0;
		for (i = 0; i + offset < cnt; i++) {
			if (offset > 0) {
				indices[i] = indices[i + offset];
			}
			vec3 dist = positions[indices[i]] - center;
			if (glm::dot(dist, dist) > sphererRadiusSquared) {
				offset++;
				i--;
			}
		}

		cnt = i;

		/* Colour the matches*/
		for (i = 0; i < cnt; i++) {
			mats[indices[i]] = POINT_TAKEN;
		}

		/* Store the results to be cleared later. */
		probeResults[index].insert(probeResults[index].begin(), indices, indices + cnt);

	} while (cnt > 0);
	qtree->searchFinish(query);

	pointCloud->update();
}

inline void printControls() {
	printf(
			R"(
Controlls:
 - WASD/left mouse drag : Pan the camera.
 - right mouse drag : Move the sphere under the cursor.
 - T : toggle the tree.
)");
}

void processKeys() {
	Camera* camera = renderer->camera;

	for (int i = 0; i < MAX_KEYS; i++) {
		if (!keys[i]) continue;

		float len = camera->position().z * 0.01;

		switch (i) {
			// Camera controls
			case 'w':
				camera->position(camera->position() + vec3(0, +len, 0));
				//cameraUpdate();
				break;
			case 's':
				camera->position(camera->position() + vec3(0, -len, 0));
				//cameraUpdate();
				break;
			case 'a':
				camera->position(camera->position() + vec3(-len, 0, 0));
				break;
			case 'd':
				camera->position(camera->position() + vec3(+len, 0, 0));
				break;
			case 't':
				qtObject->visible = !qtObject->visible;
				break;
			case ',':
				renderer->mode = RENDER_MODE_WIREFRAME;
				break;
			case '/':
				renderer->mode = RENDER_MODE_FILL;
				break;
		}
	}
}

void initGL(int *argc, char **argv) {
	glutInit(argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);

	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow("Games I - P1");

#ifdef __GLEW_H__
	glewExperimental = true;
	GLenum code = glewInit();
	if (GLEW_OK != code) fprintf(stderr, "GLEW Initialization error: %s\n", glewGetErrorString(code));
#endif

	glutReportErrors();
}

void displayCallback() {
	processKeys();
	renderer->renderScene();
	glutSwapBuffers();
}

void reshapeCallback(int w, int h) {
	renderer->setViewport(vec2(w, h));
	renderer->camera->aspectRatio((float) w / h);
	windowWidth = w;
	windowHeight = h;
}

void mouseCallback(int button, int state, int x, int y) {
	Camera* camera = renderer->camera;
	mouseX = x;
	mouseY = y;

	if (button == 0) {
		if (state == GLUT_UP) {
			if (draggedProbe >= 0) {
				updateProbeSphere(draggedProbe);
				//printf("Releasing probe sphere %d\n", draggedProbe);
				draggedProbe = -1;
				glutPostRedisplay();
			}
		} else {
			vec3 mousePos = vec3(x, y, 0);
			mousePos -= vec3(windowWidth / 2.0f, windowHeight / 2.0f, 0);
			mousePos.x = mousePos.x * camera->aspectRatio() / windowWidth;
			mousePos.y = -1.0 * mousePos.y / windowHeight;
			mousePos = mousePos * camera->height();
			mousePos += camera->position();
			mousePos.z = 0;

			//printf("Clicked at (%g, %g)\n", mousePos.x, mousePos.y);

			unsigned i;
			for (i = 0; i < numProbes; i++) {
				vec3 d = mousePos - probeSpheres[i]->position;
				if (glm::dot(d, d) <= sphererRadiusSquared) {
					draggedProbe = i;
					//printf("Dragging probe sphere %d\n", draggedProbe);
					break;
				}
			}
		}
	} else if (button == 2) {
		draggingScene = state == GLUT_DOWN;
	} else if (button == 3 && state != GLUT_UP) {
		camera->height(camera->height() / ZOOM_SCALE);
		pointSize *= ZOOM_SCALE;
		glPointSize(std::max(pointSize, 1.0));
		glutPostRedisplay();
	} else if (button == 4 && state != GLUT_UP) {
		camera->height(camera->height() * ZOOM_SCALE);
		pointSize /= ZOOM_SCALE;
		glPointSize(std::max(pointSize, 1.0));
		glutPostRedisplay();
	}
}

void motionCallback(int x, int y) {
	Camera* camera = renderer->camera;
	float ax;
	float ay;

	if (draggingScene) {
		if (camera->projectionType() == PROJECTION_PARALLEL) {
			ax = (float) (mouseX - x) * camera->height() * camera->aspectRatio() / windowWidth;
			ay = (float) (mouseY - y) * camera->height() / windowHeight;
		} else {
			float da = camera->position().z * (camera->horizontalFov() * 2);
			ax = (float) (mouseX - x) * da / windowWidth;
			ay = (float) (mouseY - y) * da / windowHeight;
		}

		//scene->root->rotation.y += -ax;
		//scene->root->rotation.x += ay;
		//printf("Rot %f\n", scene->root->rotation.y);

		camera->position(camera->position() + vec3(ax, -ay, 0));

		mouseX = x;
		mouseY = y;
		glutPostRedisplay();
	} else if (draggedProbe >= 0) {
		ax = (float) (mouseX - x) * camera->height() * camera->aspectRatio() / windowWidth;
		ay = (float) (mouseY - y) * camera->height() / windowHeight;
		mouseX = x;
		mouseY = y;

		probeSpheres[draggedProbe]->position -= vec3(ax, -ay, 0);
		updateProbeSphere(draggedProbe);
		glutPostRedisplay();
	}
}

void keyboardCallback(unsigned char key, int /*x*/, int /*y*/) {
	keys[key] = true;
	glutPostRedisplay();
}

void keyboardUpCallback(unsigned char key, int /*x*/, int /*y*/) {
	keys[key] = false;
	switch (key) {
		case 27:
			exit(EXIT_SUCCESS);
			break;
	}
}

Object * createSphere(colour col) {
	static Mesh * s = NULL;

	if (!s) s = SphereGenerator().create(20, 40, sphereRadius);

	return new Object(*s, new Materials::Flat(col), "Probe Sphere");
}

void createScene() {
	unsigned i;

	/**********************************
	 * Point cloud
	 */

	unsigned numPoints = 100000;
	const vec3 cloudBounds(50, 50, 0);

	pointCloud = Mesh::create(MESH_DYNAMIC);
	Material * mat = new Materials::FlatVertexColoured();
	pointCloud->setNumVertices(numPoints);
	pointCloud->setIndirectEnabled(false);
	int * mats = pointCloud->getMaterials();
	vec3 * pos = pointCloud->getPositions();

	for (i = 0; i < numPoints; i++) {
		mats[i] = POINT_CLEAR;
		pos[i].x = (float) rand();
		pos[i].y = (float) rand();
		pos[i].x = sin(pos[i].x) * sin(pos[i].y) * cloudBounds.x / 2;
		pos[i].y = cos(pos[i].y) * cos(pos[i].x) * cloudBounds.y / 2;
		pos[i].z = 0;
	}

	pointCloud->update();
	scene->addToRoot(new SimpleGL::Object(*pointCloud, mat, "Point Cloud"));

	/**********************************
	 * Quadtree
	 */

	qtree = new Quadtree(pos, numPoints, 15, 8);
	Mesh * qMesh = qtree->getMesh();

	mat = new Materials::Flat(colour(0xFFFFFF22));

	numPoints = qMesh->getNumVertices();
	mats = qMesh->getMaterials();

	for (i = 0; i < numPoints; i++) {
		mats[i] = 0;
	}
	qMesh->update();

	qtObject = new SimpleGL::Object(*qMesh, mat, "Quadtree Mesh");
	qtObject->visible = false;
	scene->addToRoot(qtObject);

	/**********************************
	 * Test Mesh
	 */

	mat = new Materials::Flat(colour(0x0000FFFF));

	testMesh = Mesh::create(MESH_DYNAMIC);
	testMesh->setNumVertices(numProbes);
	testMesh->setIndirectEnabled(false);
	mats = testMesh->getMaterials();
	pos = testMesh->getPositions();

	for (i = 0; i < numProbes; i++) {
		mats[i] = 0;
		pos[i].x = (float) rand() / RAND_MAX;
		pos[i].y = (float) rand() / RAND_MAX;
		pos[i].x = pos[i].x * cloudBounds.x - cloudBounds.x / 2;
		pos[i].y = pos[i].y * cloudBounds.y - cloudBounds.y / 2;
		pos[i].z = 0;

		probeSpheres[i] = createSphere(0x0000FF66);
		probeSpheres[i]->position = pos[i];
		//printf("Probe sphere %d at (%g, %g)\n", i, pos[i].x, pos[i].y);
		updateProbeSphere(i);
		scene->addToRoot(probeSpheres[i]);
	}
	testMesh->update();

	scene->addToRoot(new SimpleGL::Object(*testMesh, mat, "Probe Centers"));
}

int main(int argc, char **argv) {
	puts(
			"Quadtree Lookup Demo:\n\tEach Blue sphere is a probe area that can be moved around, the points in yellow are then looked up against the probe and the ones that match are rendered in light blue.");

// init OpenGL
	initGL(&argc, argv);
	glutDisplayFunc(displayCallback);
	glutReshapeFunc(reshapeCallback);
	glutMouseFunc(mouseCallback);
	glutMotionFunc(motionCallback);
	glutKeyboardFunc(keyboardCallback);
	glutKeyboardUpFunc(keyboardUpCallback);

// print controls
	printControls();

// create the scene
	scene = new Scene();
	scene->background = colour(0x000000FF);
	Light * l = new Light(LIGHT_TYPE_POINT_LINEAR, vec4(0, 0, 2, 0), colour(0xFFFFFFFF));
	l->size = 4.0;
	scene->lights.push_back(l);
	probeSpheres = new Object *[numProbes];
	probeResults = new std::deque<int>[numProbes];
	createScene();

// create the renderer
	renderer = SceneRenderer::create();
	renderer->scene = scene;

	Camera * cam = new Camera();
	cam->projectionType(PROJECTION_PARALLEL);
	cam->height(10);
	cam->position(vec3(0, 0, 10));
	cam->lookAt(vec3(), vec3(0, 1, 0));
	renderer->camera = cam;

//renderer->camera->setProjectionType(Graphics::Camera::Parallel);
	renderer->mode = RENDER_MODE_FILL;

	renderer->renderScene();
	glutSwapBuffers();

	glutMainLoop();
	return 0;
}
