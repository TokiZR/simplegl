#include <util/gl.h>

#include <objects/scene.h>
#include <render/renderer.h>
#include <boundtree/boundtree.h>
#include <mesh/sphere.h>
#include <mesh/cube.h>

#include <materials/flat.h>
#include <materials/flat-vertex-coloured.h>
#include <materials/untextured-lit.h>

#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <stdlib.h>
#include <algorithm>
#include <deque>
#include <chrono>
#include <thread>

using namespace SimpleGL;

/* Camera */
const float CAMERA_RES = 0.01f;
const float ZOOM_SCALE = 1.05f;

#define MAX_VELOCITY 3.0f
#define SPHERE_RADIUS 0.5f
#define SPHERE_RADIUSQ ((2 * SPHERE_RADIUS) * (2 * SPHERE_RADIUS))

#define SPHERE_BASE_COL 0xFFCC00AA

/**
 * Scene
 */
SceneRenderer * renderer;
Scene * scene;
bool draggingScene = false;
bool rotatingScene = true;
double pointSize = 0.1;

/* Window size */
int windowWidth = 800;
int windowHeight = 600;

/* Mouse */
int mouseX, mouseY;

/* Keyboard */
const int MAX_KEYS = 256;
bool keys[MAX_KEYS];

/* Camera */
float cameraDistance = 10;

using namespace std::chrono;

time_point<high_resolution_clock> lastFrame;

bounds spaceBounds(vec3(-10), vec3(10));

struct Rigidbody {
	Object * o;
	vec3 velocity;

	Rigidbody() :
			Rigidbody(NULL) {

	}

	Rigidbody(Object * obj) :
			o(obj) {
		velocity = vec3(rand(), rand(), rand());
		velocity /= RAND_MAX;
		velocity = velocity * (MAX_VELOCITY * 2) - vec3(MAX_VELOCITY);
	}

	void operator =(Object * o) {
		this->o = o;
	}

	Object * operator->() {
		return o;
	}

	Object * operator*() {
		return o;
	}

	operator Object *() {
		return o;
	}

	void addForce(vec3 f, float dur) {
		velocity += f * dur;
	}
};

std::vector<Rigidbody> spheres;

/* Tree */
Boundtree * btree;
Object * tree;

Object * createSphere(colour col) {
	static Mesh * s = NULL;
	static unsigned sp = 0;

	if (!s)
		s = SphereGenerator().create(10, 20, 0.5);

	char b[64];
	sprintf(b, "Sphere %d", sp++);
	spheres.push_back(new Object(*s, new Materials::Flat(col), b));
	return spheres.back();
}

vec3 randomPosition(bounds range) {
	vec3 r = vec3(rand(), rand(), rand());
	r /= RAND_MAX;

	r *= range.max - range.min;

	return r + range.min;
}

void removeSphere(unsigned index) {
	if (index >= spheres.size())
		return;
	scene->root->removeChild(spheres[index]);
	btree->remove(spheres[index]);
	delete spheres[index];
	spheres.erase(spheres.begin() + index);
}

void addSphere() {
	if (spheres.size() < 10000) {
		Object * s = createSphere(0xFFCC00AA);
		s->position = randomPosition(bounds(spaceBounds).enlarge(-5));
		//printf("New Sphere at (%g, %g, %g)\n", s->position.x, s->position.y, s->position.z);
		btree->insert(s);
		scene->addToRoot(s);
	}
}
inline void printControls() {
	printf(
			R"(
Controlls:
 - WASD/left mouse drag : Pan the camera
 - right mouse drag : Rotate the scene.
 - T : toggle the tree.
 - I : insert new sphere (up to 10).
 - R followed by number key: removed the given sphere.
 - C : Clear red spheres (those who have collided).
)");
}

void initGL(int *argc, char **argv) {
	glutInit(argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE | GLUT_MULTISAMPLE);

	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow("Games I - P1");

#ifdef __GLEW_H__
	glewExperimental = true;
	GLenum code = glewInit();
	if (GLEW_OK != code)
		fprintf(stderr, "GLEW Initialization error: %s\n", glewGetErrorString(code));
#endif

	glutReportErrors();
}

void displayCallback() {
	renderer->renderScene();
	glutSwapBuffers();
}

void reshapeCallback(int w, int h) {
	renderer->setViewport(vec2(w, h));
	renderer->camera->aspectRatio((float) w / h);
	windowWidth = w;
	windowHeight = h;
}

void mouseCallback(int button, int state, int x, int y) {
	Camera* camera = renderer->camera;
	mouseX = x;
	mouseY = y;

	if (button == 0) {
		rotatingScene = state == GLUT_DOWN;
	} else if (button == 2) {
		draggingScene = state == GLUT_DOWN;
	} else if (button == 3 && state != GLUT_UP) {
		double odlD = cameraDistance;
		cameraDistance /= ZOOM_SCALE;
		camera->translateFilm(vec3(0, 0, cameraDistance - odlD));
		pointSize *= ZOOM_SCALE;
		glPointSize(std::max(pointSize, 1.0));
		//glutPostRedisplay();
	} else if (button == 4 && state != GLUT_UP) {
		double odlD = cameraDistance;
		cameraDistance *= ZOOM_SCALE;
		camera->translateFilm(vec3(0, 0, cameraDistance - odlD));
		pointSize /= ZOOM_SCALE;
		glPointSize(std::max(pointSize, 1.0));
		//glutPostRedisplay();
	}
}

void motionCallback(int x, int y) {
	Camera* camera = renderer->camera;
	float ax;
	float ay;

	if (draggingScene) {
		if (camera->projectionType() == PROJECTION_PARALLEL) {
			ax = (float) (mouseX - x) * camera->height() * camera->aspectRatio() / windowWidth;
			ay = (float) (mouseY - y) * camera->height() / windowHeight;
		} else {
			float da = camera->position().z * (camera->horizontalFov() * 2);
			ax = (float) (mouseX - x) * da / windowWidth;
			ay = (float) (mouseY - y) * da / windowHeight;
		}

		//scene->root->rotation.y += -ax;
		//scene->root->rotation.x += ay;
		//printf("Rot %f\n", scene->root->rotation.y);

		camera->translateFilm(vec3(ax, -ay, 0));

		mouseX = x;
		mouseY = y;
		//glutPostRedisplay();
	} else if (rotatingScene) {
		if (camera->projectionType() == PROJECTION_PARALLEL) {
			ax = (float) (mouseX - x) * camera->height() * camera->aspectRatio() / windowWidth;
			ay = (float) (mouseY - y) * camera->height() / windowHeight;
		} else {
			float da = camera->position().z * (camera->horizontalFov() * 2);
			ax = (float) (mouseX - x) * da / windowWidth;
			ay = (float) (mouseY - y) * da / windowHeight;
		}

		scene->root->rotation.y += ax / 2;
		scene->root->rotation.x -= ay / 2;

		mouseX = x;
		mouseY = y;
		//glutPostRedisplay();
	}
}

void keyboardCallback(unsigned char key, int /*x*/, int /*y*/) {
	Camera* camera = renderer->camera;
	float len = camera->position().z * 0.01;

	switch (key) {
		// Camera controls
		case 'w':
			camera->position(camera->position() + vec3(0, +len, 0));
			//cameraUpdate();
			break;
		case 's':
			camera->position(camera->position() + vec3(0, -len, 0));
			//cameraUpdate();
			break;
		case 'a':
			camera->position(camera->position() + vec3(-len, 0, 0));
			break;
		case 'd':
			camera->position(camera->position() + vec3(+len, 0, 0));
			break;
	}

	//glutPostRedisplay();
}

void keyboardUpCallback(unsigned char key, int /*x*/, int /*y*/) {
	static bool removing = false;

	if (removing) {
		switch (key) {
			case '0' ... '9':
				//printf("Removing sphere %d\n", key - '0');
				removeSphere(key - '0');
				/* Trigger a mesh update */
				btree->getMesh();
				break;
		}
		removing = false;
	} else {
		switch (key) {
			case 't':
				tree->visible = !tree->visible;
				break;
			case 'i':
				addSphere();
				/* Trigger a mesh update */
				btree->getMesh();
				break;
			case 'r':
				removing = true;
				break;
			case 'c':
				for (Rigidbody & r : spheres) {
					Materials::Flat *f = dynamic_cast<Materials::Flat *>(r->materials[0]);
					if (f)
						f->col(SPHERE_BASE_COL);
				}
				break;
			case ',':
				renderer->mode = RENDER_MODE_WIREFRAME;
				break;
			case '/':
				renderer->mode = RENDER_MODE_FILL;
				break;
		}
	}

	//glutPostRedisplay();
}

void idleCallback() {
	static unsigned reinserts = 0;
	static unsigned frames = 0;
	static time_point<high_resolution_clock> lastMinute;

	unsigned i, cnt;
	/**********************************
	 * The physics.
	 */
	time_point<high_resolution_clock> thisFrame = high_resolution_clock::now();
	float time = duration_cast<milliseconds>(thisFrame - lastFrame).count();
	lastFrame = thisFrame;
	time /= 1000;
	//printf("Idle\n");

	/* Adjust positions */
	for (Rigidbody & r : spheres) {
		r.o->position += r.velocity * time;
		if (btree->update(r)) {
			reinserts++;
		}
	}

	/* Check collisions. */
	for (Rigidbody & r : spheres) {
		Object * b[10]; /* Big enough coz i do not want to bother */
		Boundtree::Query * q = btree->searchPrepare(r->getExtents());
		cnt = btree->searchGet(q, b, 10);

		for (i = 0; i < cnt; i++) {
			if (*r == b[i])
				continue;
			vec3 d = b[i]->position - r->position;

			if (glm::dot(d, d) <= SPHERE_RADIUSQ) {
				/* Compute resulting forces */
				r.velocity = glm::reflect(r.velocity, glm::normalize(d));
				//printf("%s collided with %s\n", r->getName().data(), b[i]->getName().data());
				Materials::Flat *f = dynamic_cast<Materials::Flat *>(r->materials[0]);
				if (f)
					f->col(0xFF000088);
			}
		}

		btree->searchFinish(q);

		if (r->position.x + SPHERE_RADIUS > spaceBounds.max.x) {
			r.addForce(vec3(2.0f * -r.velocity.x, 0, 0), 1);
		}

		if (r->position.y + SPHERE_RADIUS > spaceBounds.max.y) {
			r.addForce(vec3(0, 2.0f * -r.velocity.y, 0), 1);
		}

		if (r->position.z + SPHERE_RADIUS > spaceBounds.max.z) {
			r.addForce(vec3(0, 0, 2.0f * -r.velocity.z), 1);
		}

		if (r->position.x - SPHERE_RADIUS < spaceBounds.min.x) {
			r.addForce(vec3(2.0f * -r.velocity.x, 0, 0), 1);
		}

		if (r->position.y - SPHERE_RADIUS < spaceBounds.min.y) {
			r.addForce(vec3(0, 2.0f * -r.velocity.y, 0), 1);
		}

		if (r->position.z - SPHERE_RADIUS < spaceBounds.min.z) {
			r.addForce(vec3(0, 0, 2.0f * -r.velocity.z), 1);
		}

		//r.o->position += r.velocity * time;
	}

	btree->getMesh();

	glutPostRedisplay();
	frames++;

	if (duration_cast<seconds>(thisFrame - lastMinute).count() >= 1) {
		printf("\33[2K\r%u frames, %u reinsertions, %lu spheres, %u treeNodes.", frames, reinserts,
				spheres.size(), btree->getNumNodes());
		reinserts = 0;
		frames = 0;
		lastMinute = thisFrame;
	}

	/*std::chrono::milliseconds dura(16);
	 std::this_thread::sleep_for(dura);*/
}

void createScene() {
	unsigned i;

	btree = new Boundtree(-1, 8);

	scene->addToRoot(
			new Object(*CubeGenerator().create(spaceBounds), new Materials::Flat(0xFFFFFFFF),
					"Scene Bounds"));

	for (i = 0; i < 500; i++) {
		addSphere();
	}

	tree = new Object(*btree->getMesh(), new Materials::FlatVertexColoured(), "Tree Mesh");

	scene->addToRoot(tree);
	srand(0);
}

int main(int argc, char **argv) {
// init OpenGL
	initGL(&argc, argv);
	glutDisplayFunc(displayCallback);
	glutReshapeFunc(reshapeCallback);
	glutMouseFunc(mouseCallback);
	glutMotionFunc(motionCallback);
	glutKeyboardFunc(keyboardCallback);
	glutKeyboardUpFunc(keyboardUpCallback);
	glutIdleFunc(idleCallback);

// print controls
	printControls();

// create the scene
	scene = new Scene();
	scene->background = colour(0x000000FF);
	Light * l = new Light(LIGHT_TYPE_POINT_LINEAR, vec4(0, 0, 2, 0), colour(0xFFFFFFFF));
	l->size = 4.0;
	scene->lights.push_back(l);
	createScene();

// create the renderer
	renderer = SceneRenderer::create();
	renderer->scene = scene;

	Camera * cam = new Camera();
	//cam->projectionType(PROJECTION_PARALLEL);
	cam->height(10);
	cam->horizontalFov(M_PI * (100) / 360.0);
	cam->position(vec3(0, 0, cameraDistance));
	cam->lookAt(vec3(), vec3(0, 1, 0));
	renderer->camera = cam;

	bounds b;

	b.glob(vec3(1));

//renderer->camera->setProjectionType(Graphics::Camera::Parallel);
	renderer->mode = RENDER_MODE_FILL;

	renderer->renderScene();
	glutSwapBuffers();

	lastFrame = high_resolution_clock::now();

	setvbuf(stdout, NULL, _IONBF, 0);

	glutMainLoop();
	return 0;
}
