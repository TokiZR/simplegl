#version 330

/*******************************************************************************
 *
 *	Light Struct.
 *
 */
#define LIGHT_OFF					0
#define LIGHT_FALLOFF_LINEAR		1
#define LIGHT_FALLOFF_QUADRATIC		2
//#define LIGHT_FALLOFF_EXPONENTIAL	3

in vec3 fPosition;
vec3 normal;
in vec3 fNormal;

/**
 * Materials
 */
uniform vec3 ambients;
uniform vec3 diffuses;
uniform float shines;
uniform vec3 spots;

/**
 * Lighting
 */
uniform vec4 lightPositions[8];
uniform vec3 lightColours[8];
uniform float lightSizes[8];
uniform int lightTypes[8];
uniform int activeLights;
uniform vec3 ambientLight;
/**
 * Camera
 *
 * Homogeneous coordinates.
 */
uniform vec4 cameraPosition;

/*******************************************************************************
 *
 *	Output variables.
 *
 */
out vec4 fragmentColor;

vec3 calculateLightIntensity(in vec4 pos, in vec3 colour, in float size, in int fallof) {
	if(pos.w == 0) {
		return colour;
	} else {
		float dist = length(fPosition - pos.xyz);
		if(fallof == LIGHT_FALLOFF_LINEAR) {
			return colour * (size/dist);
		} else {
			return colour * (size/(dist * dist));
		}
	}
}

vec3 computeLightModel () {
	vec3 colour;
	vec3 reflection;
	vec3 lightRay;
	vec3 viewDir;

	if(cameraPosition.w == 0.0) {
		viewDir = - cameraPosition.xyz;
	} else {
		viewDir = fPosition - cameraPosition.xyz;
	}
	viewDir = normalize(viewDir);

	colour = diffuse * vec3(0.3, 0.3, 0.3);

	for(int i = 0; i < activeLights; i++) {
		if(lightPositions[i].w == 0)
			lightRay = -normalize( -lightPositions[i].xyz);
		else
			lightRay = -normalize((fPosition - lightPositions[i].xyz));

		reflection = reflect(lightRay, normal);

		colour += clamp(calculateLightIntensity(lightPositions[i], lightColours[i], lightSizes[i], lightTypes[i]) *
				(diffuse * dot(normal, lightRay)
				+ spot * pow(clamp(dot(viewDir, reflection), 0, 1f), (shine)) ), vec3(0), vec3(1.0));
	}

	colour = clamp(colour, vec3(0), vec3(1.0));
	return colour;
}

void main() {
	normal = normalize(fNormal);
	fragmentColor = vec4(diffuse, 1.0);
}