#version 120

/* Input */
varying vec3 fPosition;

uniform vec4 colour;

/* Output */
//out vec4 fragmentColor;

void main() {
	gl_FragColor = colour;
}