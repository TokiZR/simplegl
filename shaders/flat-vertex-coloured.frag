#version 120

/* Input */
varying vec3 fPosition;
varying vec4 fMaterialCol;

/* Output */
//out vec4 fragmentColor;

void main() {
	gl_FragColor = fMaterialCol;
}