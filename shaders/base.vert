#version 120
#extension GL_ARB_explicit_attrib_location : enable

/*******************************************************************************
 *
 *	Layout macros.
 *
 * These macros are defined to the same values in the source code.
 * 
 */
#define VERTEX_COORDS_LOCATION			0
#define VERTEX_NORMAL_LOCATION			1
#define VERTEX_MATERIAL_INDEX_LOCATION	2

/*******************************************************************************
 *
 *	Input variables.
 *
 */

layout (location = VERTEX_COORDS_LOCATION) in vec3 position;
layout (location = VERTEX_NORMAL_LOCATION) in vec3 normal;
layout (location = VERTEX_MATERIAL_INDEX_LOCATION) in int materialIndex;

/**
 * Model View Projection matrix.
 */
uniform mat4 mvpMatrix;
uniform mat4 inverseTransformMatrix;

varying vec3 fPosition;
varying vec3 fNormal;
varying vec4 fMaterialCol;


void main() {
	fPosition = position;
	fNormal = (inverseTransformMatrix * vec4(normal, 0)).xyz;
	fMaterialCol = vec4(
		(materialIndex / (256 * 256)) / 255.0,
		mod((materialIndex / (256)) , 256) / 255.0,
		mod(materialIndex , 256) / 255.0,
		1.0
	);
	//fMaterialCol = vec4((materialIndex / (256 * 256)) / 255.0);
	gl_Position = mvpMatrix * vec4(position, 1.0);
}
